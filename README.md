![GitHub Logo](/Images/documentation-Presenter.png)

## **Project Description**
Welcome to <b>Coctail Magician</b>, a web application for managing bars, their lovely customers and last but not least their delicious coctails!<br />

The purpose of our application is to free all the people from the faked reputation of most of Bars in Sofia. We would like to present to you a free community with experts and ordinary peoplee with their pure ratings and reviews <br/>
Coctail Magician is a community app and we would like to invite every person who has always paid attention for unique presentations and flavours in a Coctail.<br />
- [x] REST API
- [x] Areas:
  - public part (accessible without authentication)
  - private part (available for registered users)
  - administration part (available for admin users only)

## **Preliminary Information**
This project was assigned to <b>Ivaylo Gramatikov</b>by himself during his time while he was looking for a .NET/FULL Stack JUNIOR Position<br>

|![](/Images/Readme_logo1.png)|<div align="center"> ![512x397](/Images/Readme_logo3.png)</div>|![512x397](/Images/Readme_logo2.png)|
|:------------:|:------------:|:------------:|
|[**Telerik Academy**](https://www.telerikacademy.com/)|[**Gitlab**](https://gitlab.com/xzikoto)<br>|[**LinkedIn**](https://www.linkedin.com/in/ivaylo-gramatikov-a6338b132/)