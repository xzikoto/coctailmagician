﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CoctailMagician.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: false),
                    isBanned = table.Column<bool>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Bars",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 40, nullable: false),
                    Location = table.Column<string>(maxLength: 100, nullable: true),
                    Info = table.Column<string>(maxLength: 1000, nullable: false),
                    ImagePath = table.Column<string>(nullable: true),
                    GoogleMapsURL = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bars", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Coctails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 40, nullable: false),
                    Info = table.Column<string>(maxLength: 1000, nullable: false),
                    ImagePath = table.Column<string>(nullable: true),
                    IsAvailable = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Coctails", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Ingredients",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 25, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ingredients", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(maxLength: 128, nullable: false),
                    ProviderKey = table.Column<string>(maxLength: 128, nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    LoginProvider = table.Column<string>(maxLength: 128, nullable: false),
                    Name = table.Column<string>(maxLength: 128, nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BarRatings",
                columns: table => new
                {
                    BarId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Value = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BarRatings", x => new { x.BarId, x.UserId });
                    table.ForeignKey(
                        name: "FK_BarRatings_Bars_BarId",
                        column: x => x.BarId,
                        principalTable: "Bars",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BarRatings_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BarReviews",
                columns: table => new
                {
                    BarId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Body = table.Column<string>(maxLength: 500, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BarReviews", x => new { x.UserId, x.BarId });
                    table.ForeignKey(
                        name: "FK_BarReviews_Bars_BarId",
                        column: x => x.BarId,
                        principalTable: "Bars",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BarReviews_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BarsCoctails",
                columns: table => new
                {
                    CoctailId = table.Column<int>(nullable: false),
                    BarId = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BarsCoctails", x => new { x.CoctailId, x.BarId });
                    table.ForeignKey(
                        name: "FK_BarsCoctails_Bars_BarId",
                        column: x => x.BarId,
                        principalTable: "Bars",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BarsCoctails_Coctails_CoctailId",
                        column: x => x.CoctailId,
                        principalTable: "Coctails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CoctailRatings",
                columns: table => new
                {
                    CoctailId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Value = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoctailRatings", x => new { x.UserId, x.CoctailId });
                    table.ForeignKey(
                        name: "FK_CoctailRatings_Coctails_CoctailId",
                        column: x => x.CoctailId,
                        principalTable: "Coctails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CoctailRatings_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CoctailReviews",
                columns: table => new
                {
                    CoctailId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Review = table.Column<string>(maxLength: 500, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoctailReviews", x => new { x.CoctailId, x.UserId });
                    table.ForeignKey(
                        name: "FK_CoctailReviews_Coctails_CoctailId",
                        column: x => x.CoctailId,
                        principalTable: "Coctails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CoctailReviews_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CoctailIngredients",
                columns: table => new
                {
                    CoctailId = table.Column<int>(nullable: false),
                    IngredientId = table.Column<int>(nullable: false),
                    isDeleted = table.Column<bool>(nullable: false),
                    DeletedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoctailIngredients", x => new { x.CoctailId, x.IngredientId });
                    table.ForeignKey(
                        name: "FK_CoctailIngredients_Coctails_CoctailId",
                        column: x => x.CoctailId,
                        principalTable: "Coctails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CoctailIngredients_Ingredients_IngredientId",
                        column: x => x.IngredientId,
                        principalTable: "Ingredients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { 2, "8bbd1107-bcf5-46ba-95a4-9ec94edf9de1", "Member", "MEMBER" },
                    { 1, "4036ed8e-64dd-4ad0-8be3-b19c7d0239a6", "Manager", "MANAGER" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "CreatedOn", "DeletedOn", "Email", "EmailConfirmed", "IsDeleted", "LockoutEnabled", "LockoutEnd", "ModifiedOn", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName", "isBanned" },
                values: new object[,]
                {
                    { 10, 0, "bc970e56-9f33-4828-a996-197d809885f1", new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(230), null, "JoanneMartin@Gmail.Com", false, false, false, null, null, null, null, null, null, false, null, false, "JoanneMartin@Gmail.Com", false },
                    { 8, 0, "7a08796e-5b10-4168-b901-d6da6173cb91", new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(223), null, "AdamHamilton@Gmail.Com", false, false, false, null, null, null, null, null, null, false, null, false, "AdamHamilton@Gmail.Com", false },
                    { 7, 0, "ba55b106-2edb-481b-8bb7-435c1aecbc0f", new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(220), null, "JakeEdmunds@Gmail.Com", false, false, false, null, null, null, null, null, null, false, null, false, "JakeEdmunds@Gmail.Com", false },
                    { 6, 0, "a597eb7d-bc88-4041-851c-6ab3fbed5d8f", new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(217), null, "LaurenRees@Gmail.Com", false, false, false, null, null, null, null, null, null, false, null, false, "LaurenRees@Gmail.Com", false },
                    { 5, 0, "6d43767d-59a2-4111-82c4-67131721e8ad", new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(207), null, "EdwardLewis@Gmail.Com", false, false, false, null, null, null, null, null, null, false, null, false, "EdwardLewis@Gmail.Com", false },
                    { 4, 0, "a636f9b2-1fc5-4a7d-8a24-8308b840f459", new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(203), null, "SoniaArnold@Gmail.Com", false, false, false, null, null, null, null, null, null, false, null, false, "SoniaArnold@Gmail.Com", false },
                    { 3, 0, "556dc54b-3be1-47da-952a-447d6dcfd9db", new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(178), null, "KatherineHoward@Gmail.Com", false, false, false, null, null, null, null, null, null, false, null, false, "KatherineHoward@Gmail.Com", false },
                    { 2, 0, "dd0e0ac6-f68b-4b19-9a71-1a77a12919d5", new DateTime(2020, 11, 9, 8, 43, 45, 950, DateTimeKind.Utc).AddTicks(9340), null, "JonathanRandall@Gmail.Com", false, false, false, null, null, null, null, null, null, false, null, false, "JonathanRandall@Gmail.Com", false },
                    { 1, 0, "66a9f6d0-1314-42f8-af49-c223bfe57723", new DateTime(2020, 11, 9, 8, 43, 45, 939, DateTimeKind.Utc).AddTicks(6772), null, "manager@bo.com", false, false, true, null, null, "MANAGER@BO.COM", "MANAGER@BO.COM", "AQAAAAEAACcQAAAAEFug7dA4y912XbxHaNxhVgufh0KXZdn1YdeyeXx2/hl/gvlTiGiNqpYChJO8whaXxg==", null, false, "7I5VNHIJTSZNOT3KDWKNFUV5PVYBHGXN", false, "manager@bo.com", false },
                    { 9, 0, "076c8cb6-6d62-4d4e-bc54-e6f8b14ce8cc", new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(227), null, "OwenMetcalfe@Gmail.Com", false, false, false, null, null, null, null, null, null, false, null, false, "OwenMetcalfe@Gmail.Com", false }
                });

            migrationBuilder.InsertData(
                table: "Bars",
                columns: new[] { "Id", "CreatedOn", "DeletedOn", "GoogleMapsURL", "ImagePath", "Info", "IsDeleted", "Location", "ModifiedOn", "Name", "Phone" },
                values: new object[,]
                {
                    { 1, new DateTime(2020, 11, 9, 8, 43, 45, 952, DateTimeKind.Utc).AddTicks(732), null, "https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d9930.13386419354!2d-0.0794724!3d51.5217746!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xfb717a4393ae1f7!2sThe%20Allegory!5e0!3m2!1sen!2sbg!4v1573065214726!5m2!1sen!2sbg", "/assets/img/bars/the-allegory.jpg", "In the heart of Shoreditch's happening hub, Principal Place, The Allegory is an everyday escape in London’s buzzing unsquare mile. Linger over a long brunch before finishing with an espresso martini.Enjoy quick catch-ups over pastries and freshly ground coffee, wholesome sharing platters and creative cocktails with colleagues; these one-of-a-kind experiences will be found at The Allegory. With a beautiful alfresco terrace, large open plan bar and cosy candlelit corners, this is a destination you'll want to return to again and again.", false, "1a Principal Place, Worship Street, London, EC2A 2BA", null, "The Allegory", "020 3948 9810" },
                    { 6, new DateTime(2020, 11, 9, 8, 43, 45, 952, DateTimeKind.Utc).AddTicks(4200), null, "https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d9929.097022005784!2d-0.0877761!3d51.5265294!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xffe5bb4f98cd7af9!2sNightjar!5e0!3m2!1sen!2sbg!4v1574664621210!5m2!1sen!2sbg", "/assets/img/bars/nightjar.jpg", "Our flagship bar & restaurant located in the heart of London's most exciting dining destination with two floors, two show stopping bars, an open kitchen and an extensive year-round outside space. Perfect for alfresco dining and drinks in the sun. Nightjar offers a relaxed drinking and dining space in a beautiful setting. Open from an early morning until late evening, it's perfect for every occasion - from a business meeting and working lunch to a romantic dinner or after work drinks.", false, "129 City Rd, London, EC1V 1JB", null, "Nightjar", "0207 253 4101" },
                    { 11, new DateTime(2020, 11, 9, 8, 43, 45, 952, DateTimeKind.Utc).AddTicks(4248), null, "https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d9930.120715470508!2d-0.085644!3d51.5218349!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb4a28ec580aab28!2sAviary%20-%20Rooftop%20Restaurant%20%26%20Terrace%20Bar!5e0!3m2!1sen!2sbg!4v1574860809123!5m2!1sen!2sbg", "/assets/img/bars/aviary.jpg", "Nestled between the City and Shoreditch, the 6,000 sq ft rooftop dining destination designed by Russell Sage Studio features an impressive central bar which elegantly divides the stylish restaurant area from the opulent bar lounge. The eclectic interiors bring the outdoors in with hanging planters alongside gold drinks cases, plush single seating and relaxed banquettes creating a bright, vibrant vibe.", false, "22-25 Finsbury Square, London, EC2A 1DX", null, "Aviary", "020 3873 4060" },
                    { 10, new DateTime(2020, 11, 9, 8, 43, 45, 952, DateTimeKind.Utc).AddTicks(4246), null, "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2483.3029276456064!2d-0.0260607840281346!3d51.5076582185051!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487602b6306f0be9%3A0x793dfd9d17079629!2sThe%20Sipping%20Room!5e0!3m2!1sen!2sbg!4v1573065491821!5m2!1sen!2sbg", "/assets/img/bars/the-sipping-room.jpg", "An escape from the everyday, The Sipping Room specialises in thoughtful, inspired menus, locally sourced ingredients, and innovative, handcrafted cocktails. Retreat from the world while you enjoy our unrivalled service in the most welcoming environment. Our stylish outdoor terrace provides the perfect alfresco respite throughout the seasons.", false, "16 Hertsmere Road, London, E14 4AX", null, "The Sipping Room", "020 3907 0320" },
                    { 9, new DateTime(2020, 11, 9, 8, 43, 45, 952, DateTimeKind.Utc).AddTicks(4205), null, "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2483.0790771431516!2d-0.13363628402805472!3d51.511765218204765!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487604d25acdb76d%3A0xc5c854eeaaa62990!2sOpium%20Cocktail%20bar%20and%20Dim%20Sum%20Parlour!5e0!3m2!1sen!2sbg!4v1573065470157!5m2!1sen!2sbg", "/assets/img/bars/opium.jpg", "Opium Cocktail Bar & Dim Sum Parlour is a chic, hidden venue in the heart of Chinatown. Run by experienced London bar moguls Dre Masso and Eric Yu, Opium certainly has some pedigree behind it. The decor is oriental themed but again keeps an element of freshness with a twist that makes it modern and current; metal finishes on miss-matched Chinese furniture gives Opium, a contemporary London feel that is very welcome. Expect 3 bars of amazing Asian cocktails and a selection of dim sum - just a little teaser to get your appetite going.", false, "5-16 Gerrard Street, London, W1D 6JE", null, "Opium", "020 7734 7276" },
                    { 8, new DateTime(2020, 11, 9, 8, 43, 45, 952, DateTimeKind.Utc).AddTicks(4203), null, "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39734.535778488505!2d-0.05813209602882269!3d51.50572144844429!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487602b7642f2f9d%3A0x19f521ba29dd3f1a!2sThe%20Parlour!5e0!3m2!1sen!2sbg!4v1573065440869!5m2!1sen!2sbg", "/assets/img/bars/the-parlour.jpg", "Located in the Park Pavilion on Canada Square, Canary Wharf, The Parlour is a striking & innovative all-day bar with style, substance & seasonally tempting drinks and food. A secret garden-inspired lounge with timber panelling is a must for cocktail lovers & perfect for pre- or post-dinner drinks, whilst the mixology table is ideal for those who want to mix & muddle for themselves. A stunning alfresco terrace, complete with its own bar provides the perfect playpen for those wanting to soak up the sun.", false, "The Park Pavilion, London, E14 5FW", null, "The Parlour", "0207 715 9551" },
                    { 7, new DateTime(2020, 11, 9, 8, 43, 45, 952, DateTimeKind.Utc).AddTicks(4202), null, "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2483.4112946113805!2d-0.025301484028207793!3d51.50566991865038!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487602b7be6a030f%3A0x93dcb32d47e3e562!2sThe%20Pagination!5e0!3m2!1sen!2sbg!4v1573065408325!5m2!1sen!2sbg", "/assets/img/bars/the-pagination.jpg", "Perfectly positioned on the riverside in Canary Wharf, next to the bridge leading over to West India Quay, The Pagination is the perfect antidote to busy London life. With industrial inspired details, exposed metals, and soft handwoven textures, it offers a sanctuary, day or night and the expansive terrace offers alfresco drinking and dining in both the warmer months and the colder due to the abundance of blankets and hot water bottles to keep you snug.", false, "9 Cabot Square, London, E14 4EB", null, "The Pagination", "020 7512 0397" },
                    { 2, new DateTime(2020, 11, 9, 8, 43, 45, 952, DateTimeKind.Utc).AddTicks(4125), null, "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d9931.461092179858!2d-0.12384975375194002!3d51.515687677335066!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487604af32e9d343%3A0x422a8e0b815341b!2sThe%20Refinery!5e0!3m2!1sen!2sbg!4v1573065262411!5m2!1sen!2sbg", "/assets/img/bars/the-refinery.jpg", "Statement wallpapers and furniture are complemented by soft lighting and cosy faux fur to create your uber chic, contemporary bar - The Refinery CityPoint. The all-day dining bar & restaurant features a private dining room, sunken lounge and alfresco terrace with a pizza oven in the summer. It suits all occasions from early morning breakfasts right through to late night drinks. Make the most of our set menus for larger groups, or pre order packages when you want a selection of nibbles to eat!", false, "1 Ropemaker Street, London, EC2Y 9HT", null, "The Refinery", "020 7382 0606" },
                    { 3, new DateTime(2020, 11, 9, 8, 43, 45, 952, DateTimeKind.Utc).AddTicks(4194), null, "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2482.7646347568057!2d-0.1070710840279029!3d51.517533917782934!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761b52ee3d7ad9%3A0xa677a8c2b71574ca!2sThe%20Fable!5e0!3m2!1sen!2sbg!4v1573065312782!5m2!1sen!2sbg", "/assets/img/bars/the-fable.jpg", "Inspired by the fantasy world of fairy tales and Aesop's fables, The Fable near Holborn Viaduct in central London, is anything but ordinary. From the vintage typewriter, to the leather bound books, every detail tells a story. Whether you visit for crafted cocktails, a morning latte & eggs Benedict or dinner at dusk, expect to be entranced, enthralled and enchanted.", false, "52 Holborn Viaduct, London, EC1A 2FD", null, "The Fable", "0207 651 4940" },
                    { 4, new DateTime(2020, 11, 9, 8, 43, 45, 952, DateTimeKind.Utc).AddTicks(4196), null, "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2482.788117828657!2d-0.0803738840278907!3d51.517103117814386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761cb30d422641%3A0x2c7c1dfd5e33c70!2sDevonshire%20Terrace!5e0!3m2!1sen!2sbg!4v1573065339941!5m2!1sen!2sbg", "/assets/img/bars/devonshire-terrace.jpg", "In the heart of the peaceful Devonshire Square, moments from Liverpool Street Station, Devonshire Terrace is your everyday escape from the hustle and bustle of City life. From quick catch-ups over freshly ground coffee to relaxing after work cocktails in one of our many gorgeous spaces, sit back and relax and we'll take care of the rest. No need to wait for the warmer months to drink and dine alfresco, enjoy our all year round terrace with its beautiful glass domed roof to protect you from the elements.", false, "Devonshire Terrace, Devonshire Square, London, EC2M 4WY", null, "Devonshire Terrace", "020 7256 3233" },
                    { 5, new DateTime(2020, 11, 9, 8, 43, 45, 952, DateTimeKind.Utc).AddTicks(4198), null, "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2482.788117828657!2d-0.0803738840278907!3d51.517103117814386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487603552b6c7317%3A0x8e08163a80221ab9!2sThe%20Anthologist!5e0!3m2!1sen!2sbg!4v1573065358702!5m2!1sen!2sbg", "/assets/img/bars/the-anthologist.jpg", "Located right in the heart of the City, The Anthologist is the ideal backdrop for all your drink and food needs, from breakfast meetings to client updates over lunch, after work drinks or dinner with friends. Sample new wines or vintages from across the globe, a unique range of innovative cocktails and relaxed all - day dining fare.", false, "58 Gresham Street, London, EC2V 7BB", null, "The Anthologist", "0207 726 8711" }
                });

            migrationBuilder.InsertData(
                table: "Coctails",
                columns: new[] { "Id", "CreatedOn", "DeletedOn", "ImagePath", "Info", "IsAvailable", "IsDeleted", "ModifiedOn", "Name" },
                values: new object[,]
                {
                    { 2, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(7052), null, "/assets/img/cocktails/passionfruit-martini.jpg", "This easy passion fruit cocktail is bursting with zingy flavours and is perfect for celebrating with friends. Top with prosecco for a special tipple", null, false, null, "Passion fruit martini" },
                    { 1, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(3842), null, "/assets/img/cocktails/cosmopolitan-cocktail.jpg", "Lipsmackingly sweet-and-sour, the Cosmopolitan cocktail of vodka, cranberry, orange liqueur and citrus is a good time in a glass. Perfect for a party.", null, false, null, "Cosmopolitan cocktail" },
                    { 3, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(7111), null, "/assets/img/cocktails/raspberry-gin.jpg", "Preserve the taste of summer in a bottle with this raspberry gin, perfect topped up with tonic. The gin will keep its lovely pink hue for a few months", null, false, null, "Raspberry gin" },
                    { 10, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(7124), null, "/assets/img/cocktails/cranberry-vodka.jpg", "This bittersweet fruity vodka is best served well chilled in shot glasses. It can also be made with other berries like blackcurrants or strawberries.", null, false, null, "Cranberry vodka" },
                    { 9, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(7122), null, "/assets/img/cocktails/bucks-fizz.jpg", "The simple and classic combination of orange juice and champagne makes a perfect cocktail for a celebratory brunch or party", null, false, null, "Bucks fizz" },
                    { 8, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(7120), null, "/assets/img/cocktails/wine-sangria.jpg", "Try this refreshing twist on a traditional sangria and use white wine instead of red with elderflower to complement the fruit. Perfect for summer parties.", null, false, null, "White wine sangria" },
                    { 7, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(7118), null, "/assets/img/cocktails/michelada.jpg", "Cold lager, chilli powder, pepper and lime: spice up your lager with this Mexican cocktail, popular throughout Latin America and great for a summer party.", null, false, null, "Michelada" },
                    { 6, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(7117), null, "/assets/img/cocktails/long-island-ice-tea.jpg", "Mix a jug of this classic cocktail for a summer party. It's made with equal parts of vodka, gin, tequila, rum and triple sec, plus lime, cola and plenty of ice.", null, false, null, "Long Island iced tea" },
                    { 5, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(7115), null, "/assets/img/cocktails/pink-gin-iced-tea.jpg", "Blend pink gin with iced tea and you have this unique cocktail, made with spiced rum, elderflower and pink grapefruit. Serve in a jug for a sharing cocktail.", null, false, null, "Pink gin iced tea" },
                    { 4, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(7113), null, "/assets/img/cocktails/sex-on-the-beach.jpg", "Combine vodka with peach schnapps and cranberry juice to make a classic sex on the beach cocktail. Garnish with cocktail cherries and orange slices.", null, false, null, "Sex on the beach cocktail" }
                });

            migrationBuilder.InsertData(
                table: "Ingredients",
                columns: new[] { "Id", "CreatedOn", "DeletedOn", "IsDeleted", "ModifiedOn", "Name" },
                values: new object[,]
                {
                    { 25, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(2757), null, false, null, "Kiwis" },
                    { 24, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(2756), null, false, null, "Apples" },
                    { 23, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(2755), null, false, null, "Olives" },
                    { 22, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(2754), null, false, null, "Cherries" },
                    { 21, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(2753), null, false, null, "Oranges" },
                    { 20, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(2752), null, false, null, "Peach schnapps" },
                    { 15, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(2747), null, false, null, "Passoa" },
                    { 18, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(2750), null, false, null, "Raspberries" },
                    { 1, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(1473), null, false, null, "Whisky" },
                    { 2, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(2706), null, false, null, "Gin" },
                    { 3, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(2733), null, false, null, "Vodka" },
                    { 4, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(2734), null, false, null, "Rum" },
                    { 5, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(2735), null, false, null, "Tequila" },
                    { 6, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(2736), null, false, null, "Cointreau" },
                    { 7, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(2738), null, false, null, "Cola" },
                    { 9, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(2740), null, false, null, "Club soda" },
                    { 10, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(2741), null, false, null, "Lemon Sour" },
                    { 11, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(2742), null, false, null, "Cinnamon Syrup" },
                    { 12, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(2744), null, false, null, "Triple sec" },
                    { 13, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(2745), null, false, null, "Cranberry juice" },
                    { 14, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(2746), null, false, null, "Lime wedge" },
                    { 16, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(2748), null, false, null, "Sugar syrup" },
                    { 17, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(2749), null, false, null, "Prosecco" },
                    { 19, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(2751), null, false, null, "Strawberries" },
                    { 8, new DateTime(2020, 11, 9, 8, 43, 45, 951, DateTimeKind.Utc).AddTicks(2739), null, false, null, "Ginger ale" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[] { 1, 1 });

            migrationBuilder.InsertData(
                table: "BarRatings",
                columns: new[] { "BarId", "UserId", "CreatedOn", "DeletedOn", "IsDeleted", "ModifiedOn", "Value" },
                values: new object[,]
                {
                    { 4, 5, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(7059), null, false, null, 4.0 },
                    { 5, 6, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(7061), null, false, null, 4.0 },
                    { 1, 2, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(7030), null, false, null, 4.0 },
                    { 8, 2, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(7067), null, false, null, 4.0 },
                    { 3, 4, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(7056), null, false, null, 4.0 },
                    { 6, 7, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(7063), null, false, null, 2.0 },
                    { 2, 3, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(7054), null, false, null, 5.0 },
                    { 9, 3, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(7069), null, false, null, 3.0 },
                    { 7, 8, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(7065), null, false, null, 4.0 }
                });

            migrationBuilder.InsertData(
                table: "BarReviews",
                columns: new[] { "UserId", "BarId", "Body", "CreatedOn", "DeletedOn", "IsDeleted", "ModifiedOn" },
                values: new object[,]
                {
                    { 7, 9, "I really like this place and had my birthday drinks here after work.  Staff are accommodating, reasonable priced and the outside seating is great", new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(5967), null, false, null },
                    { 7, 6, "Get there early if you want to have a table or stool as it gets full very fast and most tables are pre reserved.", new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(5953), null, false, null },
                    { 7, 2, "It’s quite a nice place to go for some drinks and food after work, which is what I did. Although a little cold, there are blankets and warm things to keep you warm under the heaters. Food was good though we was told that the menu we was not the correct one, then was waiting around for ages.", new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(5933), null, false, null },
                    { 5, 8, "Quite good.", new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(5963), null, false, null },
                    { 6, 8, "Good lunchtime menu.. tasty meal, good price, great service and environment. Separate area for food or just drinks", new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(5965), null, false, null },
                    { 6, 5, "1 star because 0 doesn't seem to be an option... I had the misfortune of visiting for lunch and will never make that mistake again.I have been here for drinks before and while service has never been good it's never been noticably terrible before.", new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(5949), null, false, null },
                    { 8, 3, "I went on Tuesday of the last week and the weather was horrible so I really like the detail of having some blankets outside and also the waitress was super nice and attentive. Best place in Canary wharf!", new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(5935), null, false, null },
                    { 6, 2, "Sure, I am happy to provide more information. I visited for a work event yesterday evening. The place is good with some nice outside space. Drink options are ok/alright. Music was a big negative though ; as this was a work event we were there to network and talk. This was made impossible as the music volume suddenly (around 7.30pm) was increased dramatically making it impossible to continue any conversation.", new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(5931), null, false, null },
                    { 8, 9, "One of my favourite bars!", new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(5969), null, false, null },
                    { 8, 6, "I been there few times and I can say that everything is fantastic. Especially Tony is a very good guy... I will defenitly come back.", new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(5955), null, false, null },
                    { 5, 5, "Open space, light, modern place. Cosy. No table cloth, table was a bit dirty. Friendly caring service. Food OK - nice BF omlette, but toast too crunchy and dry.", new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(5947), null, false, null },
                    { 3, 7, "Everything is good with this venue - except the food.", new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(5959), null, false, null },
                    { 2, 7, "Nice seating area outside. Great place for a happy hour. Very friendly staff. Just didn't like the burgers we've ordered. Not really tasty. May try something else next time.", new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(5957), null, false, null },
                    { 3, 3, "Awsome music and drinks.", new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(5939), null, false, null },
                    { 3, 1, "Great bar to visit after work. We have been visiting after work most Fridays and some weekdays for the best part of 2 years now. Good atmosphere, great decor and all round friendly staff. Highly recommended.", new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(5917), null, false, null },
                    { 3, 10, "Nice inside with a reel modern feel. Cocktails were great!", new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(6018), null, false, null },
                    { 2, 10, "Good place, a bit expensive though!", new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(5971), null, false, null },
                    { 5, 2, "Love this place. I've been coming here for 3 years and the staff are wonderful. Amazing service, great location and menu.  They are very dog friendly and treat mine like their own... Highly recommend this place.", new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(5929), null, false, null },
                    { 2, 5, "Highly recommended to anyone looking for breakfast in the Canary Wharf district. Full veggie breakfast and buttermilk pancakes perfectly executed, with punctual service and a cozy atmosphere. Would go again.", new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(5951), null, false, null },
                    { 3, 4, "One of Canary wharf's more relaxing venues for a breakfast meeting.", new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(5943), null, false, null },
                    { 2, 3, "Stuff are friendly, location is charming.", new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(5937), null, false, null },
                    { 2, 1, "I must be honest I picked the place because I have never been there. I was by myself and the plan for me was to drink as I needed a night by myself to think and forget everything around me. The barman (by the accent was French) was freaking cool.asked a lot of questions, suggested drinks and was quick and nice to me all night long.", new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(5458), null, false, null },
                    { 4, 1, "I stumbled across while visiting Canary Wharf and I have to say I was really impressed by how nice and accomodating the staff were. I received a very warm welcome straight from walking through the doors, they looked like they had some sort of event in the evening as all the tables were reserved but they let me seat before the party arrived. I ordered for a beer and some nibbles which came quick and was really tasty. Excactly what I needed after a long day visiting and shopping.", new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(5926), null, false, null },
                    { 4, 4, "A dog wandering around sniffing my legs on a Friday night in a well designed bar?", new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(5945), null, false, null },
                    { 4, 7, "We instantly felt welcome as soon as we walked in the door. The food was amazing and the staff were very warm and friendly and nothing was  too much trouble. The decor is stunning and I thoroughly recommend this bar/restaurant. We will definitely be going back.", new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(5961), null, false, null },
                    { 4, 10, "Stylish and great outdoor areas. Expensive.", new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(6020), null, false, null },
                    { 2, 4, "I organised my office Christmas party at the pagination and it was the most epic night ever. The staff were super efficient and executed my plan to the detail. In addition the venue is so nicely decorated for the Christmas holiday. I'm not a fan of vegan food but they didn't have a wide selection, if you aren't fussy then it's a place to check out.", new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(5941), null, false, null }
                });

            migrationBuilder.InsertData(
                table: "BarsCoctails",
                columns: new[] { "CoctailId", "BarId", "CreatedOn" },
                values: new object[,]
                {
                    { 10, 11, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3213) },
                    { 10, 9, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3198) },
                    { 10, 8, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3190) },
                    { 10, 7, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3182) },
                    { 10, 3, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3157) },
                    { 9, 8, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3186) },
                    { 4, 8, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3184) },
                    { 4, 4, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3161) },
                    { 4, 1, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3147) },
                    { 3, 3, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3160) },
                    { 3, 6, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3175) },
                    { 3, 1, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3144) },
                    { 2, 11, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3214) },
                    { 4, 11, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3220) },
                    { 2, 10, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3211) },
                    { 2, 6, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3172) },
                    { 2, 2, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3148) },
                    { 2, 1, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3126) },
                    { 1, 10, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3209) },
                    { 1, 9, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3202) },
                    { 1, 6, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3177) },
                    { 1, 7, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3181) },
                    { 2, 9, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3193) },
                    { 5, 5, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3168) },
                    { 5, 4, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3165) },
                    { 5, 9, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3200) },
                    { 9, 10, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3204) },
                    { 9, 6, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3174) },
                    { 9, 2, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3150) },
                    { 8, 8, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3188) },
                    { 8, 4, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3163) },
                    { 8, 5, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3167) },
                    { 7, 10, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3206) },
                    { 7, 9, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3195) },
                    { 7, 2, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3152) },
                    { 7, 5, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3170) },
                    { 7, 3, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3155) },
                    { 6, 11, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3216) },
                    { 6, 10, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3207) },
                    { 6, 8, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3191) },
                    { 6, 7, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3179) },
                    { 6, 3, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3154) },
                    { 5, 11, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3218) },
                    { 9, 9, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(3197) },
                    { 1, 1, new DateTime(2020, 11, 9, 11, 43, 45, 952, DateTimeKind.Local).AddTicks(6224) }
                });

            migrationBuilder.InsertData(
                table: "CoctailIngredients",
                columns: new[] { "CoctailId", "IngredientId", "DeletedOn", "isDeleted" },
                values: new object[,]
                {
                    { 7, 8, null, false },
                    { 6, 10, null, false },
                    { 4, 3, null, false },
                    { 9, 3, null, false },
                    { 10, 3, null, false },
                    { 6, 4, null, false },
                    { 2, 3, null, false },
                    { 7, 10, null, false },
                    { 10, 13, null, false },
                    { 8, 3, null, false },
                    { 1, 14, null, false },
                    { 1, 3, null, false },
                    { 3, 18, null, false },
                    { 8, 19, null, false },
                    { 4, 21, null, false },
                    { 5, 25, null, false },
                    { 5, 2, null, false },
                    { 3, 2, null, false },
                    { 2, 14, null, false },
                    { 6, 3, null, false }
                });

            migrationBuilder.InsertData(
                table: "CoctailRatings",
                columns: new[] { "UserId", "CoctailId", "CreatedOn", "DeletedOn", "IsDeleted", "ModifiedOn", "Value" },
                values: new object[,]
                {
                    { 7, 9, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(9175), null, false, null, 2.0 },
                    { 3, 7, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(9166), null, false, null, 5.0 },
                    { 2, 3, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(9179), null, false, null, 4.0 },
                    { 5, 6, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(9171), null, false, null, 4.0 },
                    { 4, 5, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(9169), null, false, null, 4.0 },
                    { 2, 8, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(9092), null, false, null, 4.0 },
                    { 3, 2, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(9181), null, false, null, 3.0 },
                    { 6, 10, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(9173), null, false, null, 4.0 },
                    { 8, 4, new DateTime(2020, 11, 9, 11, 43, 45, 953, DateTimeKind.Local).AddTicks(9177), null, false, null, 4.0 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_BarRatings_UserId",
                table: "BarRatings",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_BarReviews_BarId",
                table: "BarReviews",
                column: "BarId");

            migrationBuilder.CreateIndex(
                name: "IX_BarsCoctails_BarId",
                table: "BarsCoctails",
                column: "BarId");

            migrationBuilder.CreateIndex(
                name: "IX_CoctailIngredients_IngredientId",
                table: "CoctailIngredients",
                column: "IngredientId");

            migrationBuilder.CreateIndex(
                name: "IX_CoctailRatings_CoctailId",
                table: "CoctailRatings",
                column: "CoctailId");

            migrationBuilder.CreateIndex(
                name: "IX_CoctailReviews_UserId",
                table: "CoctailReviews",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "BarRatings");

            migrationBuilder.DropTable(
                name: "BarReviews");

            migrationBuilder.DropTable(
                name: "BarsCoctails");

            migrationBuilder.DropTable(
                name: "CoctailIngredients");

            migrationBuilder.DropTable(
                name: "CoctailRatings");

            migrationBuilder.DropTable(
                name: "CoctailReviews");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Bars");

            migrationBuilder.DropTable(
                name: "Ingredients");

            migrationBuilder.DropTable(
                name: "Coctails");

            migrationBuilder.DropTable(
                name: "AspNetUsers");
        }
    }
}
