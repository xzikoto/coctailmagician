﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoctailMagician.Data.Entities
{
    public class CoctailIngredient
    {
        //public int Id { get; set; }

        public Coctail Coctail { get; set; }
        public int CoctailId { get; set; }
        
        public Ingredient Ingredient { get; set; }
        public int IngredientId { get; set; }

        public bool isDeleted { get; set; } = false;
        public DateTime? DeletedOn { get; set; }

    }
}
