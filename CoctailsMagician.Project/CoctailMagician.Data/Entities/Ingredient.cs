﻿using CoctailMagician.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CoctailMagician.Data.Entities
{
    public class Ingredient: Entity
    {
        public Ingredient()
        {
            CocktailIngredients = new List<CoctailIngredient>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        [DisplayName("Ingredient Name")]
        [Required]
        [StringLength(25, ErrorMessage = "The {0} value cannot exceed {1} characters.")]
        public string Name { get; set; }

        public ICollection<CoctailIngredient> CocktailIngredients { get; set; }
    }
}
