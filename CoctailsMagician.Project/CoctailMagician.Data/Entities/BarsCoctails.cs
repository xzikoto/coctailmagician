﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoctailMagician.Data.Entities
{
    public class BarsCoctails
    {
        public int CoctailId { get; set; }
        public Coctail Coctail { get; set; }

        public int BarId { get; set; }
        public Bar Bar { get; set; }

        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;
    }
}
