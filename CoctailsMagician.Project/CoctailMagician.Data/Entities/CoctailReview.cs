﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Text;

namespace CoctailMagician.Data.Entities
{
    public class CoctailReview:  CoctailFeedback
    {
        [Required]
        [MaxLength(500, ErrorMessage = "Text cannot exceed {1} characters")]
        public string Review { get; set; }
    }
}
