﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CoctailMagician.Data.Entities
{
    public class BarRating : BarFeedback
    {
        [Required]
        [Range(1, 5, ErrorMessage = "Please enter value between {0} and {1}")]
        public double Value { get; set; }
    }
}
