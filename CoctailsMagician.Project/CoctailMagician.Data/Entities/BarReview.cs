﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CoctailMagician.Data.Entities
{
    public class BarReview: BarFeedback
    {
        [Required]
        [MaxLength(500, ErrorMessage = "Text cannot exceed {1} characters")]
        public string Body { get; set; }
    }
}
