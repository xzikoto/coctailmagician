﻿using CoctailMagician.Data.Entities.Abstract;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CoctailMagician.Data.Entities
{
    public class User : IdentityUser<int>
    {
        [Key]
        public override int Id { get; set; }
        [Required]
        [MinLength(3), MaxLength(128)]
        public override string UserName { get => base.UserName; set => base.UserName = value; }

        public bool isBanned { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDeleted { get; set; }
    }
}
