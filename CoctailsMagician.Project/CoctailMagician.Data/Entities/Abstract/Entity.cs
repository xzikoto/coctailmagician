﻿using System;
using CoctailMagician.Data.Entities.Contracts;

namespace CoctailMagician.Data.Entities.Abstract
{
    public abstract class Entity : IAuditable, IDeletable
    {
        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDeleted { get; set; }
    }
}
