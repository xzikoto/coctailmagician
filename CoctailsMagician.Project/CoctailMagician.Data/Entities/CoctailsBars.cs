﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoctailMagician.Data.Entities
{
    public class CoctailsBars
    {
        public int CoctailId { get; set; }
        public Coctail Coctail { get; set; }
    }
}
