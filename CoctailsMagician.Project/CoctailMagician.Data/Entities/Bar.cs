﻿using CoctailMagician.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CoctailMagician.Data.Entities
{
    public class Bar: Entity
    {
        public Bar()
        {
            BarsCoctails = new List<BarsCoctails>();
            Reviews = new List<BarReview>();
        }
        
        public int Id { get; set; }

        [DisplayName("Bar Name")]
        [Required]
        [StringLength(40, ErrorMessage = "The {0} value cannot exceed {1} characters.")]
        public string Name { get; set; }
        

        [StringLength(100, ErrorMessage = "The {0} value cannot exceed {1} characters.")]
        public string Location { get; set; }

        [DisplayName("Bar Info")]
        [Required]
        [StringLength(1000, ErrorMessage = "The {0} value cannot exceed {1} characters.")]
        public string Info { get; set; }
        
        public string ImagePath { get; set; }

        public string GoogleMapsURL { get; set; }

        [Required]
        [MinLength(9, ErrorMessage = "The {0} value cannot be under {1} characters.")]
        public string Phone { get; set; }

        public ICollection<BarRating> Ratings { get; set; } = new List<BarRating>();
        public ICollection<BarReview> Reviews { get; set; }
        public ICollection<BarsCoctails> BarsCoctails { get; set; }
    }
}
