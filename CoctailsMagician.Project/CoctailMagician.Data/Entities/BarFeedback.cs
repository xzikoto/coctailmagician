﻿using CoctailMagician.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CoctailMagician.Data.Entities
{
    public abstract class BarFeedback: Entity
    {
        public Bar Bar { get; set; }
        public int BarId { get; set; }

        public User User { get; set; }
        public int UserId { get; set; }
    }
}
