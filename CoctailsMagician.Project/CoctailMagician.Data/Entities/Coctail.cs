﻿using CoctailMagician.Data.Entities.Abstract;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CoctailMagician.Data.Entities
{
    public class Coctail :  Entity
    {
        public Coctail()
        {
            this.BarCoctails = new List<BarsCoctails>();
            this.Ingredients = new List<CoctailIngredient>();
            this.Ratings = new List<CoctailRating>();
            this.Reviews = new List<CoctailReview>();
        }

        public int Id { get; set; }

        [DisplayName("Coctail Name")]
        [Required]
        [StringLength(40,ErrorMessage ="The {0} value cannot exceed {1} characters")]
        public string Name { get; set; }

        [DisplayName("Coctail Info")]
        [Required]
        [StringLength(1000, ErrorMessage = "The {0} value cannot exceed {1} characters")]
        public string Info { get; set; }

        public string ImagePath { get; set; }

        public bool? IsAvailable { get; set; }

        [Required]
        public List<CoctailIngredient> Ingredients { get; set; }
        public List<CoctailRating> Ratings { get; set; }
        public List<CoctailReview> Reviews { get; set; }
        public List<BarsCoctails> BarCoctails { get; set; }
    }
}
