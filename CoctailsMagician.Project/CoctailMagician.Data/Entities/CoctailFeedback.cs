﻿using System;
using CoctailMagician.Data.Entities.Abstract;
using System.Collections.Generic;
using System.Text;

namespace CoctailMagician.Data.Entities
{
    public abstract class CoctailFeedback : Entity
    {
        public Coctail Coctail { get; set; }
        public int CoctailId { get; set; }

        public User User { get; set; }
        public int UserId { get; set; }
    }
}
