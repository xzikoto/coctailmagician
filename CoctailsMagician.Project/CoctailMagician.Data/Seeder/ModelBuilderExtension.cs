﻿using CoctailMagician.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoctailMagician.Data.Seeder
{
    public static class ModelBuilderExtension
    {
        public static void Seeder(this ModelBuilder builder)
        {

            //SEEDING ROLES
            builder.Entity<Role>().HasData(
                new Role { Id = 1, Name = "Manager", NormalizedName = "MANAGER" },
                new Role { Id = 2, Name = "Member", NormalizedName = "MEMBER" }
            );


            //SEEDING MANAGER ACCOUNT
            var hasher = new PasswordHasher<User>();

            User managerUser = new User
            {
                Id = 1,
                UserName = "manager@bo.com",
                NormalizedUserName = "MANAGER@BO.COM",
                Email = "manager@bo.com",
                NormalizedEmail = "MANAGER@BO.COM",
                CreatedOn = DateTime.UtcNow,
                LockoutEnabled = true,
                SecurityStamp = "7I5VNHIJTSZNOT3KDWKNFUV5PVYBHGXN"
            };

            managerUser.PasswordHash = hasher.HashPassword(managerUser, "P@ssw0rd");

            builder.Entity<User>().HasData(managerUser);

            builder.Entity<IdentityUserRole<int>>().HasData(
                new IdentityUserRole<int>
                {
                    RoleId = 1,
                    UserId = managerUser.Id
                });


            //SEEDING USERS
            var user1 = new User
            {
                Id = 2,
                UserName = "JonathanRandall@Gmail.Com",
                Email = "JonathanRandall@Gmail.Com",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false,
                isBanned = false
            };

            var user2 = new User
            {
                Id = 3,
                UserName = "KatherineHoward@Gmail.Com",
                Email = "KatherineHoward@Gmail.Com",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false,
                isBanned = false
            };

            var user3 = new User
            {
                Id = 4,
                UserName = "SoniaArnold@Gmail.Com",
                Email = "SoniaArnold@Gmail.Com",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false,
                isBanned = false
            };

            var user4 = new User
            {
                Id = 5,
                UserName = "EdwardLewis@Gmail.Com",
                Email = "EdwardLewis@Gmail.Com",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false,
                isBanned = false
            };

            var user5 = new User
            {
                Id = 6,
                UserName = "LaurenRees@Gmail.Com",
                Email = "LaurenRees@Gmail.Com",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false,
                isBanned = false
            };

            var user6 = new User
            {
                Id = 7,
                UserName = "JakeEdmunds@Gmail.Com",
                Email = "JakeEdmunds@Gmail.Com",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false,
                isBanned = false
            };

            var user7 = new User
            {
                Id = 8,
                UserName = "AdamHamilton@Gmail.Com",
                Email = "AdamHamilton@Gmail.Com",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false,
                isBanned = false
            };

            var user8 = new User
            {
                Id = 9,
                UserName = "OwenMetcalfe@Gmail.Com",
                Email = "OwenMetcalfe@Gmail.Com",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false,
                isBanned = false
            };

            var user9 = new User
            {
                Id = 10,
                UserName = "JoanneMartin@Gmail.Com",
                Email = "JoanneMartin@Gmail.Com",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false,
                isBanned = false
            };
            
            builder.Entity<User>().HasData(user1, user2, user3, user4, user5, user6, user7, user8, user9);

            //Ingredients copied from Radko Stanev
            var whisky = new Ingredient
            {
                Id = 1,
                Name = "Whisky"
            };
            var gin = new Ingredient
            {
                Id = 2,
                Name = "Gin"
            };
            var vodka = new Ingredient
            {
                Id = 3,
                Name = "Vodka"
            };
            var rum = new Ingredient
            {
                Id = 4,
                Name = "Rum"
            };
            var tequila = new Ingredient
            {
                Id = 5,
                Name = "Tequila"
            };
            var cointreau = new Ingredient
            {
                Id = 6,
                Name = "Cointreau"
            };
            var cola = new Ingredient
            {
                Id = 7,
                Name = "Cola"
            };
            var gingerAle = new Ingredient
            {
                Id = 8,
                Name = "Ginger ale"
            };
            var clubSoda = new Ingredient
            {
                Id = 9,
                Name = "Club soda"
            };
            var lemonSour = new Ingredient
            {
                Id = 10,
                Name = "Lemon Sour"
            };
            var cinnamonSyrup = new Ingredient
            {
                Id = 11,
                Name = "Cinnamon Syrup"
            };
            var tripleSec = new Ingredient
            {
                Id = 12,
                Name = "Triple sec"
            };
            var cranberryJuice = new Ingredient
            {
                Id = 13,
                Name = "Cranberry juice"
            };
            var limeWedge = new Ingredient
            {
                Id = 14,
                Name = "Lime wedge"
            };
            var passoa = new Ingredient
            {
                Id = 15,
                Name = "Passoa"
            };
            var sugarSyrup = new Ingredient
            {
                Id = 16,
                Name = "Sugar syrup"
            };
            var prosecco = new Ingredient
            {
                Id = 17,
                Name = "Prosecco"
            };
            var raspberries = new Ingredient
            {
                Id = 18,
                Name = "Raspberries"
            };
            var strawberries = new Ingredient
            {
                Id = 19,
                Name = "Strawberries"
            };
            var peachSchnapps = new Ingredient
            {
                Id = 20,
                Name = "Peach schnapps"
            };
            var oranges = new Ingredient
            {
                Id = 21,
                Name = "Oranges"
            };
            var cherries = new Ingredient
            {
                Id = 22,
                Name = "Cherries"
            };
            var olives = new Ingredient
            {
                Id = 23,
                Name = "Olives"
            };
            var apples = new Ingredient
            {
                Id = 24,
                Name = "Apples"
            };
            var kiwis = new Ingredient
            {
                Id = 25,
                Name = "Kiwis"
            };

            builder.Entity<Ingredient>().HasData(whisky, gin, vodka, rum, tequila, cointreau, cola, gingerAle, clubSoda, lemonSour, cinnamonSyrup, tripleSec, cranberryJuice, limeWedge, passoa, sugarSyrup, prosecco, raspberries, strawberries, peachSchnapps, oranges, cherries, olives, apples, kiwis);


            //SEEDING COCKTAILS
            var cosmopolitan = new Coctail
            {
                Id = 1,
                Name = "Cosmopolitan cocktail",
                Info = "Lipsmackingly sweet-and-sour, the Cosmopolitan cocktail of vodka, cranberry, orange liqueur and citrus is a good time in a glass. Perfect for a party.",
                ImagePath = "/assets/img/cocktails/cosmopolitan-cocktail.jpg",
            };
            var passionFruit = new Coctail
            {
                Id = 2,
                Name = "Passion fruit martini",
                Info = "This easy passion fruit cocktail is bursting with zingy flavours and is perfect for celebrating with friends. Top with prosecco for a special tipple",
                ImagePath = "/assets/img/cocktails/passionfruit-martini.jpg",
            };
            var raspberryGin = new Coctail
            {
                Id = 3,
                Name = "Raspberry gin",
                Info = "Preserve the taste of summer in a bottle with this raspberry gin, perfect topped up with tonic. The gin will keep its lovely pink hue for a few months",
                ImagePath = "/assets/img/cocktails/raspberry-gin.jpg",
            };
            var sexOnTheBeach = new Coctail
            {
                Id = 4,
                Name = "Sex on the beach cocktail",
                Info = "Combine vodka with peach schnapps and cranberry juice to make a classic sex on the beach cocktail. Garnish with cocktail cherries and orange slices.",
                ImagePath = "/assets/img/cocktails/sex-on-the-beach.jpg",
            };
            var pinkGinIcedTea = new Coctail
            {
                Id = 5,
                Name = "Pink gin iced tea",
                Info = "Blend pink gin with iced tea and you have this unique cocktail, made with spiced rum, elderflower and pink grapefruit. Serve in a jug for a sharing cocktail.",
                ImagePath = "/assets/img/cocktails/pink-gin-iced-tea.jpg",
            };
            var longIsland = new Coctail
            {
                Id = 6,
                Name = "Long Island iced tea",
                Info = "Mix a jug of this classic cocktail for a summer party. It's made with equal parts of vodka, gin, tequila, rum and triple sec, plus lime, cola and plenty of ice.",
                ImagePath = "/assets/img/cocktails/long-island-ice-tea.jpg",
            };
            var michelada = new Coctail
            {
                Id = 7,
                Name = "Michelada",
                Info = "Cold lager, chilli powder, pepper and lime: spice up your lager with this Mexican cocktail, popular throughout Latin America and great for a summer party.",
                ImagePath = "/assets/img/cocktails/michelada.jpg",
            };
            var whiteSangria = new Coctail
            {
                Id = 8,
                Name = "White wine sangria",
                Info = "Try this refreshing twist on a traditional sangria and use white wine instead of red with elderflower to complement the fruit. Perfect for summer parties.",
                ImagePath = "/assets/img/cocktails/wine-sangria.jpg",
            };
            var bucksFizz = new Coctail
            {
                Id = 9,
                Name = "Bucks fizz",
                Info = "The simple and classic combination of orange juice and champagne makes a perfect cocktail for a celebratory brunch or party",
                ImagePath = "/assets/img/cocktails/bucks-fizz.jpg",
            };
            var cranberryVodka = new Coctail
            {
                Id = 10,
                Name = "Cranberry vodka",
                Info = "This bittersweet fruity vodka is best served well chilled in shot glasses. It can also be made with other berries like blackcurrants or strawberries.",
                ImagePath = "/assets/img/cocktails/cranberry-vodka.jpg",
            };

            builder.Entity<Coctail>().HasData(cosmopolitan, passionFruit, raspberryGin, sexOnTheBeach, pinkGinIcedTea, longIsland, michelada, whiteSangria, bucksFizz, cranberryVodka);

            //Seeding Coctail Ingredients
            //SEEDING COCKTAILINGREDIENTS
            var cocktailIngredient1 = new CoctailIngredient
            {
                CoctailId = cosmopolitan.Id,
                IngredientId = vodka.Id,
            };

            var cocktailIngredient2 = new CoctailIngredient
            {
                CoctailId = cosmopolitan.Id,
                IngredientId = limeWedge.Id,
            };

            var cocktailIngredient3 = new CoctailIngredient
            {
                CoctailId = passionFruit.Id,
                IngredientId = vodka.Id,
            };

            var cocktailIngredient4 = new CoctailIngredient
            {
                CoctailId = passionFruit.Id,
                IngredientId = limeWedge.Id,
            };

            var cocktailIngredient5 = new CoctailIngredient
            {
                CoctailId = raspberryGin.Id,
                IngredientId = raspberries.Id,
            };

            var cocktailIngredient6 = new CoctailIngredient
            {
                CoctailId = raspberryGin.Id,
                IngredientId = gin.Id,
            };

            var cocktailIngredient7 = new CoctailIngredient
            {
                CoctailId = sexOnTheBeach.Id,
                IngredientId = vodka.Id,
            };

            var cocktailIngredient8 = new CoctailIngredient
            {
                CoctailId = sexOnTheBeach.Id,
                IngredientId = oranges.Id,
            };

            var cocktailIngredient9 = new CoctailIngredient
            {
                CoctailId = pinkGinIcedTea.Id,
                IngredientId = gin.Id,
            };

            var cocktailIngredient10 = new CoctailIngredient
            {
                CoctailId = pinkGinIcedTea.Id,
                IngredientId = kiwis.Id,
            };

            var cocktailIngredient11 = new CoctailIngredient
            {
                CoctailId = longIsland.Id,
                IngredientId = vodka.Id,
            };

            var cocktailIngredient12 = new CoctailIngredient
            {
                CoctailId = longIsland.Id,
                IngredientId = rum.Id,
            };

            var cocktailIngredient13 = new CoctailIngredient
            {
                CoctailId = longIsland.Id,
                IngredientId = lemonSour.Id,
            };

            var cocktailIngredient14 = new CoctailIngredient
            {
                CoctailId = michelada.Id,
                IngredientId = lemonSour.Id,
            };

            var cocktailIngredient15 = new CoctailIngredient
            {
                CoctailId = michelada.Id,
                IngredientId = gingerAle.Id,
            };

            var cocktailIngredient16 = new CoctailIngredient
            {
                CoctailId = whiteSangria.Id,
                IngredientId = strawberries.Id,
            };

            var cocktailIngredient17 = new CoctailIngredient
            {
                CoctailId = whiteSangria.Id,
                IngredientId = vodka.Id,
            };

            var cocktailIngredient18 = new CoctailIngredient
            {
                CoctailId = bucksFizz.Id,
                IngredientId = vodka.Id,
            };

            var cocktailIngredient19 = new CoctailIngredient
            {
                CoctailId = cranberryVodka.Id,
                IngredientId = vodka.Id,
            };

            var cocktailIngredient20 = new CoctailIngredient
            {
                CoctailId = cranberryVodka.Id,
                IngredientId = cranberryJuice.Id,
            };

            builder.Entity<CoctailIngredient>().HasData(cocktailIngredient1, cocktailIngredient2, cocktailIngredient3, cocktailIngredient4, cocktailIngredient5, cocktailIngredient6, cocktailIngredient7, cocktailIngredient8, cocktailIngredient9, cocktailIngredient10, cocktailIngredient11, cocktailIngredient12, cocktailIngredient13, cocktailIngredient14, cocktailIngredient15, cocktailIngredient16, cocktailIngredient17, cocktailIngredient18, cocktailIngredient19, cocktailIngredient20);

            //SEEDING BARS
            //SEEDING BARS
            var bar1 = new Bar
            {
                Id = 1,
                Name = "The Allegory",
                Info = "In the heart of Shoreditch's happening hub, Principal Place, The Allegory is an everyday escape in London’s buzzing unsquare mile. Linger over a long brunch before finishing with an espresso martini.Enjoy quick catch-ups over pastries and freshly ground coffee, wholesome sharing platters and creative cocktails with colleagues; these one-of-a-kind experiences will be found at The Allegory. With a beautiful alfresco terrace, large open plan bar and cosy candlelit corners, this is a destination you'll want to return to again and again.",
                Location = "1a Principal Place, Worship Street, London, EC2A 2BA",
                ImagePath = "/assets/img/bars/the-allegory.jpg",
                Phone = "020 3948 9810",
                GoogleMapsURL = "https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d9930.13386419354!2d-0.0794724!3d51.5217746!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xfb717a4393ae1f7!2sThe%20Allegory!5e0!3m2!1sen!2sbg!4v1573065214726!5m2!1sen!2sbg",
            };

            var bar2 = new Bar
            {
                Id = 2,
                Name = "The Refinery",
                Info = "Statement wallpapers and furniture are complemented by soft lighting and cosy faux fur to create your uber chic, contemporary bar - The Refinery CityPoint. The all-day dining bar & restaurant features a private dining room, sunken lounge and alfresco terrace with a pizza oven in the summer. It suits all occasions from early morning breakfasts right through to late night drinks. Make the most of our set menus for larger groups, or pre order packages when you want a selection of nibbles to eat!",
                Location = "1 Ropemaker Street, London, EC2Y 9HT",
                ImagePath = "/assets/img/bars/the-refinery.jpg",
                Phone = "020 7382 0606",
                GoogleMapsURL = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d9931.461092179858!2d-0.12384975375194002!3d51.515687677335066!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487604af32e9d343%3A0x422a8e0b815341b!2sThe%20Refinery!5e0!3m2!1sen!2sbg!4v1573065262411!5m2!1sen!2sbg",
            };
            var bar3 = new Bar
            {
                Id = 3,
                Name = "The Fable",
                Info = "Inspired by the fantasy world of fairy tales and Aesop's fables, The Fable near Holborn Viaduct in central London, is anything but ordinary. From the vintage typewriter, to the leather bound books, every detail tells a story. Whether you visit for crafted cocktails, a morning latte & eggs Benedict or dinner at dusk, expect to be entranced, enthralled and enchanted.",
                Location = "52 Holborn Viaduct, London, EC1A 2FD",
                ImagePath = "/assets/img/bars/the-fable.jpg",
                Phone = "0207 651 4940",
                GoogleMapsURL = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2482.7646347568057!2d-0.1070710840279029!3d51.517533917782934!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761b52ee3d7ad9%3A0xa677a8c2b71574ca!2sThe%20Fable!5e0!3m2!1sen!2sbg!4v1573065312782!5m2!1sen!2sbg",
            };
            var bar4 = new Bar
            {
                Id =  4,
                Name = "Devonshire Terrace",
                Info = "In the heart of the peaceful Devonshire Square, moments from Liverpool Street Station, Devonshire Terrace is your everyday escape from the hustle and bustle of City life. From quick catch-ups over freshly ground coffee to relaxing after work cocktails in one of our many gorgeous spaces, sit back and relax and we'll take care of the rest. No need to wait for the warmer months to drink and dine alfresco, enjoy our all year round terrace with its beautiful glass domed roof to protect you from the elements.",
                Location = "Devonshire Terrace, Devonshire Square, London, EC2M 4WY",
                ImagePath = "/assets/img/bars/devonshire-terrace.jpg",
                Phone = "020 7256 3233",
                GoogleMapsURL = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2482.788117828657!2d-0.0803738840278907!3d51.517103117814386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761cb30d422641%3A0x2c7c1dfd5e33c70!2sDevonshire%20Terrace!5e0!3m2!1sen!2sbg!4v1573065339941!5m2!1sen!2sbg",
            };
            var bar5 = new Bar
            {
                Id = 5,
                Name = "The Anthologist",
                Info = "Located right in the heart of the City, The Anthologist is the ideal backdrop for all your drink and food needs, from breakfast meetings to client updates over lunch, after work drinks or dinner with friends. Sample new wines or vintages from across the globe, a unique range of innovative cocktails and relaxed all - day dining fare.",
                Location = "58 Gresham Street, London, EC2V 7BB",
                ImagePath = "/assets/img/bars/the-anthologist.jpg",
                Phone = "0207 726 8711",
                GoogleMapsURL = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2482.788117828657!2d-0.0803738840278907!3d51.517103117814386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487603552b6c7317%3A0x8e08163a80221ab9!2sThe%20Anthologist!5e0!3m2!1sen!2sbg!4v1573065358702!5m2!1sen!2sbg",
            };
            var bar6 = new Bar
            {
                Id = 6,
                Name = "Nightjar",
                Info = "Our flagship bar & restaurant located in the heart of London's most exciting dining destination with two floors, two show stopping bars, an open kitchen and an extensive year-round outside space. Perfect for alfresco dining and drinks in the sun. Nightjar offers a relaxed drinking and dining space in a beautiful setting. Open from an early morning until late evening, it's perfect for every occasion - from a business meeting and working lunch to a romantic dinner or after work drinks.",
                Location = "129 City Rd, London, EC1V 1JB",
                ImagePath = "/assets/img/bars/nightjar.jpg",
                Phone = "0207 253 4101",
                GoogleMapsURL = "https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d9929.097022005784!2d-0.0877761!3d51.5265294!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xffe5bb4f98cd7af9!2sNightjar!5e0!3m2!1sen!2sbg!4v1574664621210!5m2!1sen!2sbg",
            };
            var bar7 = new Bar
            {
                Id = 7,
                Name = "The Pagination",
                Info = "Perfectly positioned on the riverside in Canary Wharf, next to the bridge leading over to West India Quay, The Pagination is the perfect antidote to busy London life. With industrial inspired details, exposed metals, and soft handwoven textures, it offers a sanctuary, day or night and the expansive terrace offers alfresco drinking and dining in both the warmer months and the colder due to the abundance of blankets and hot water bottles to keep you snug.",
                Location = "9 Cabot Square, London, E14 4EB",
                ImagePath = "/assets/img/bars/the-pagination.jpg",
                Phone = "020 7512 0397",
                GoogleMapsURL = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2483.4112946113805!2d-0.025301484028207793!3d51.50566991865038!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487602b7be6a030f%3A0x93dcb32d47e3e562!2sThe%20Pagination!5e0!3m2!1sen!2sbg!4v1573065408325!5m2!1sen!2sbg",
            };
            var bar8 = new Bar
            {
                Id = 8,
                Name = "The Parlour",
                Info = "Located in the Park Pavilion on Canada Square, Canary Wharf, The Parlour is a striking & innovative all-day bar with style, substance & seasonally tempting drinks and food. A secret garden-inspired lounge with timber panelling is a must for cocktail lovers & perfect for pre- or post-dinner drinks, whilst the mixology table is ideal for those who want to mix & muddle for themselves. A stunning alfresco terrace, complete with its own bar provides the perfect playpen for those wanting to soak up the sun.",
                Location = "The Park Pavilion, London, E14 5FW",
                ImagePath = "/assets/img/bars/the-parlour.jpg",
                Phone = "0207 715 9551",
                GoogleMapsURL = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39734.535778488505!2d-0.05813209602882269!3d51.50572144844429!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487602b7642f2f9d%3A0x19f521ba29dd3f1a!2sThe%20Parlour!5e0!3m2!1sen!2sbg!4v1573065440869!5m2!1sen!2sbg",
            };

            var bar9 = new Bar
            {
                Id = 9,
                Name = "Opium",
                Info = "Opium Cocktail Bar & Dim Sum Parlour is a chic, hidden venue in the heart of Chinatown. Run by experienced London bar moguls Dre Masso and Eric Yu, Opium certainly has some pedigree behind it. The decor is oriental themed but again keeps an element of freshness with a twist that makes it modern and current; metal finishes on miss-matched Chinese furniture gives Opium, a contemporary London feel that is very welcome. Expect 3 bars of amazing Asian cocktails and a selection of dim sum - just a little teaser to get your appetite going.",
                Location = "5-16 Gerrard Street, London, W1D 6JE",
                ImagePath = "/assets/img/bars/opium.jpg",
                Phone = "020 7734 7276",
                GoogleMapsURL = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2483.0790771431516!2d-0.13363628402805472!3d51.511765218204765!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487604d25acdb76d%3A0xc5c854eeaaa62990!2sOpium%20Cocktail%20bar%20and%20Dim%20Sum%20Parlour!5e0!3m2!1sen!2sbg!4v1573065470157!5m2!1sen!2sbg",
            };

            var bar10 = new Bar
            {
                Id = 10,
                Name = "The Sipping Room",
                Info = "An escape from the everyday, The Sipping Room specialises in thoughtful, inspired menus, locally sourced ingredients, and innovative, handcrafted cocktails. Retreat from the world while you enjoy our unrivalled service in the most welcoming environment. Our stylish outdoor terrace provides the perfect alfresco respite throughout the seasons.",
                Location = "16 Hertsmere Road, London, E14 4AX",
                ImagePath = "/assets/img/bars/the-sipping-room.jpg",
                Phone = "020 3907 0320",
                GoogleMapsURL = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2483.3029276456064!2d-0.0260607840281346!3d51.5076582185051!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487602b6306f0be9%3A0x793dfd9d17079629!2sThe%20Sipping%20Room!5e0!3m2!1sen!2sbg!4v1573065491821!5m2!1sen!2sbg",
            };

            var bar11 = new Bar
            {
                Id = 11,
                Name = "Aviary",
                Info = "Nestled between the City and Shoreditch, the 6,000 sq ft rooftop dining destination designed by Russell Sage Studio features an impressive central bar which elegantly divides the stylish restaurant area from the opulent bar lounge. The eclectic interiors bring the outdoors in with hanging planters alongside gold drinks cases, plush single seating and relaxed banquettes creating a bright, vibrant vibe.",
                Location = "22-25 Finsbury Square, London, EC2A 1DX",
                ImagePath = "/assets/img/bars/aviary.jpg",
                Phone = "020 3873 4060",
                GoogleMapsURL = "https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d9930.120715470508!2d-0.085644!3d51.5218349!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb4a28ec580aab28!2sAviary%20-%20Rooftop%20Restaurant%20%26%20Terrace%20Bar!5e0!3m2!1sen!2sbg!4v1574860809123!5m2!1sen!2sbg",
            };

            builder.Entity<Bar>().HasData(bar1, bar2, bar3, bar4, bar5, bar6, bar7, bar8, bar9, bar10, bar11);

            ////SEEDING BARCOCKTAILS 
            var barCocktail1 = new BarsCoctails
            {
                BarId = bar1.Id,
                CoctailId = cosmopolitan.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail2 = new BarsCoctails
            {
                BarId = bar1.Id,
                CoctailId = passionFruit.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail3 = new BarsCoctails
            {
                BarId = bar1.Id,
                CoctailId = raspberryGin.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail4 = new BarsCoctails
            {
                BarId = bar1.Id,
                CoctailId = sexOnTheBeach.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail5 = new BarsCoctails
            {
                BarId = bar2.Id,
                CoctailId = passionFruit.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail6 = new BarsCoctails
            {
                BarId = bar2.Id,
                CoctailId = bucksFizz.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail23 = new BarsCoctails
            {
                BarId = bar2.Id,
                CoctailId = michelada.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail7 = new BarsCoctails
            {
                BarId = bar3.Id,
                CoctailId = longIsland.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail8 = new BarsCoctails
            {
                BarId = bar3.Id,
                CoctailId = michelada.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail9 = new BarsCoctails
            {
                BarId = bar3.Id,
                CoctailId = cranberryVodka.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail24 = new BarsCoctails
            {
                BarId = bar3.Id,
                CoctailId = raspberryGin.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail10 = new BarsCoctails
            {
                BarId = bar4.Id,
                CoctailId = sexOnTheBeach.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail25 = new BarsCoctails
            {
                BarId = bar4.Id,
                CoctailId = whiteSangria.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail26 = new BarsCoctails
            {
                BarId = bar4.Id,
                CoctailId = pinkGinIcedTea.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail11 = new BarsCoctails
            {
                BarId = bar5.Id,
                CoctailId = whiteSangria.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail12 = new BarsCoctails
            {
                BarId = bar5.Id,
                CoctailId = pinkGinIcedTea.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail13 = new BarsCoctails
            {
                BarId = bar5.Id,
                CoctailId = michelada.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail14 = new BarsCoctails
            {
                BarId = bar6.Id,
                CoctailId = passionFruit.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail15 = new BarsCoctails
            {
                BarId = bar6.Id,
                CoctailId = bucksFizz.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail16 = new BarsCoctails
            {
                BarId = bar6.Id,
                CoctailId = raspberryGin.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail27 = new BarsCoctails
            {
                BarId = bar6.Id,
                CoctailId = cosmopolitan.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail17 = new BarsCoctails
            {
                BarId = bar7.Id,
                CoctailId = longIsland.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail18 = new BarsCoctails
            {
                BarId = bar7.Id,
                CoctailId = cosmopolitan.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail19 = new BarsCoctails
            {
                BarId = bar7.Id,
                CoctailId = cranberryVodka.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail20 = new BarsCoctails
            {
                BarId = bar8.Id,
                CoctailId = sexOnTheBeach.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail28 = new BarsCoctails
            {
                BarId = bar8.Id,
                CoctailId = bucksFizz.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail29 = new BarsCoctails
            {
                BarId = bar8.Id,
                CoctailId = whiteSangria.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail30 = new BarsCoctails
            {
                BarId = bar8.Id,
                CoctailId = cranberryVodka.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail31 = new BarsCoctails
            {
                BarId = bar8.Id,
                CoctailId = longIsland.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail21 = new BarsCoctails
            {
                BarId = bar9.Id,
                CoctailId = passionFruit.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail32 = new BarsCoctails
            {
                BarId = bar9.Id,
                CoctailId = michelada.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail33 = new BarsCoctails
            {
                BarId = bar9.Id,
                CoctailId = bucksFizz.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail34 = new BarsCoctails
            {
                BarId = bar9.Id,
                CoctailId = cranberryVodka.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail35 = new BarsCoctails
            {
                BarId = bar9.Id,
                CoctailId = pinkGinIcedTea.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail36 = new BarsCoctails
            {
                BarId = bar9.Id,
                CoctailId = cosmopolitan.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail22 = new BarsCoctails
            {
                BarId = bar10.Id,
                CoctailId = bucksFizz.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail37 = new BarsCoctails
            {
                BarId = bar10.Id,
                CoctailId = michelada.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail38 = new BarsCoctails
            {
                BarId = bar10.Id,
                CoctailId = longIsland.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail39 = new BarsCoctails
            {
                BarId = bar10.Id,
                CoctailId = cosmopolitan.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail40 = new BarsCoctails
            {
                BarId = bar10.Id,
                CoctailId = passionFruit.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail41 = new BarsCoctails
            {
                BarId = bar11.Id,
                CoctailId = cranberryVodka.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail42 = new BarsCoctails
            {
                BarId = bar11.Id,
                CoctailId = passionFruit.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail43 = new BarsCoctails
            {
                BarId = bar11.Id,
                CoctailId = longIsland.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail44 = new BarsCoctails
            {
                BarId = bar11.Id,
                CoctailId = pinkGinIcedTea.Id,
                CreatedOn = DateTime.Now
            };
            var barCocktail45 = new BarsCoctails
            {
                BarId = bar11.Id,
                CoctailId = sexOnTheBeach.Id,
                CreatedOn = DateTime.Now
            };

            builder.Entity<BarsCoctails>().HasData(barCocktail1, barCocktail2, barCocktail3, barCocktail4, barCocktail5, barCocktail6, barCocktail7, barCocktail8, barCocktail9, barCocktail10, barCocktail11, barCocktail12, barCocktail13, barCocktail14, barCocktail15, barCocktail16, barCocktail17, barCocktail18, barCocktail19, barCocktail20, barCocktail21, barCocktail22, barCocktail23, barCocktail24, barCocktail25, barCocktail26, barCocktail27, barCocktail28, barCocktail29, barCocktail30, barCocktail31, barCocktail32, barCocktail33, barCocktail34, barCocktail35, barCocktail36, barCocktail37, barCocktail38, barCocktail39, barCocktail40, barCocktail41, barCocktail42, barCocktail43, barCocktail44, barCocktail45);

            //Seeeding Comments
            //SEEDING BARCOMMENTS
            var barComment1 = new BarReview
            {
                BarId = bar1.Id,
                UserId = user1.Id,
                Body = "I must be honest I picked the place because I have never been there. I was by myself and the plan for me was to drink as I needed a night by myself to think and forget everything around me. The barman (by the accent was French) was freaking cool.asked a lot of questions, suggested drinks and was quick and nice to me all night long.",
                CreatedOn = DateTime.Now
            };
            var barComment2 = new BarReview
            {
                BarId = bar1.Id,
                UserId = user2.Id,
                Body = "Great bar to visit after work. We have been visiting after work most Fridays and some weekdays for the best part of 2 years now. Good atmosphere, great decor and all round friendly staff. Highly recommended.",
                CreatedOn = DateTime.Now
            };
            var barComment3 = new BarReview
            {
                BarId = bar1.Id,
                UserId = user3.Id,
                Body = "I stumbled across while visiting Canary Wharf and I have to say I was really impressed by how nice and accomodating the staff were. I received a very warm welcome straight from walking through the doors, they looked like they had some sort of event in the evening as all the tables were reserved but they let me seat before the party arrived. I ordered for a beer and some nibbles which came quick and was really tasty. Excactly what I needed after a long day visiting and shopping.",
                CreatedOn = DateTime.Now
            };
            var barComment4 = new BarReview
            {
                BarId = bar2.Id,
                UserId = user4.Id,
                Body = "Love this place. I've been coming here for 3 years and the staff are wonderful. Amazing service, great location and menu.  They are very dog friendly and treat mine like their own... Highly recommend this place.",
                CreatedOn = DateTime.Now
            };
            var barComment5 = new BarReview
            {
                BarId = bar2.Id,
                UserId = user5.Id,
                Body = "Sure, I am happy to provide more information. I visited for a work event yesterday evening. The place is good with some nice outside space. Drink options are ok/alright. Music was a big negative though ; as this was a work event we were there to network and talk. This was made impossible as the music volume suddenly (around 7.30pm) was increased dramatically making it impossible to continue any conversation.",
                CreatedOn = DateTime.Now
            };
            var barComment6 = new BarReview
            {
                BarId = bar2.Id,
                UserId = user6.Id,
                Body = "It’s quite a nice place to go for some drinks and food after work, which is what I did. Although a little cold, there are blankets and warm things to keep you warm under the heaters. Food was good though we was told that the menu we was not the correct one, then was waiting around for ages.",
                CreatedOn = DateTime.Now
            };
            var barComment7 = new BarReview
            {
                BarId = bar3.Id,
                UserId = user7.Id,
                Body = "I went on Tuesday of the last week and the weather was horrible so I really like the detail of having some blankets outside and also the waitress was super nice and attentive. Best place in Canary wharf!",
                CreatedOn = DateTime.Now
            };
            var barComment8 = new BarReview
            {
                BarId = bar3.Id,
                UserId = user1.Id,
                Body = "Stuff are friendly, location is charming.",
                CreatedOn = DateTime.Now
            };
            var barComment9 = new BarReview
            {
                BarId = bar3.Id,
                UserId = user2.Id,
                Body = "Awsome music and drinks.",
                CreatedOn = DateTime.Now
            };
            var barComment10 = new BarReview
            {
                BarId = bar4.Id,
                UserId = user1.Id,
                Body = "I organised my office Christmas party at the pagination and it was the most epic night ever. The staff were super efficient and executed my plan to the detail. In addition the venue is so nicely decorated for the Christmas holiday. I'm not a fan of vegan food but they didn't have a wide selection, if you aren't fussy then it's a place to check out.",
                CreatedOn = DateTime.Now
            };
            var barComment11 = new BarReview
            {
                BarId = bar4.Id,
                UserId = user2.Id,
                Body = "One of Canary wharf's more relaxing venues for a breakfast meeting.",
                CreatedOn = DateTime.Now
            };
            var barComment12 = new BarReview
            {
                BarId = bar4.Id,
                UserId = user3.Id,
                Body = "A dog wandering around sniffing my legs on a Friday night in a well designed bar?",
                CreatedOn = DateTime.Now
            };
            var barComment13 = new BarReview
            {
                BarId = bar5.Id,
                UserId = user4.Id,
                Body = "Open space, light, modern place. Cosy. No table cloth, table was a bit dirty. Friendly caring service. Food OK - nice BF omlette, but toast too crunchy and dry.",
                CreatedOn = DateTime.Now
            };
            var barComment14 = new BarReview
            {
                BarId = bar5.Id,
                UserId = user5.Id,
                Body = "1 star because 0 doesn't seem to be an option... I had the misfortune of visiting for lunch and will never make that mistake again.I have been here for drinks before and while service has never been good it's never been noticably terrible before.",
                CreatedOn = DateTime.Now
            };
            var barComment15 = new BarReview
            {
                BarId = bar5.Id,
                UserId = user1.Id,
                Body = "Highly recommended to anyone looking for breakfast in the Canary Wharf district. Full veggie breakfast and buttermilk pancakes perfectly executed, with punctual service and a cozy atmosphere. Would go again.",
                CreatedOn = DateTime.Now
            };
            var barComment16 = new BarReview
            {
                BarId = bar6.Id,
                UserId = user6.Id,
                Body = "Get there early if you want to have a table or stool as it gets full very fast and most tables are pre reserved.",
                CreatedOn = DateTime.Now
            };
            var barComment17 = new BarReview
            {
                BarId = bar6.Id,
                UserId = user7.Id,
                Body = "I been there few times and I can say that everything is fantastic. Especially Tony is a very good guy... I will defenitly come back.",
                CreatedOn = DateTime.Now
            };
            var barComment18 = new BarReview
            {
                BarId = bar7.Id,
                UserId = user1.Id,
                Body = "Nice seating area outside. Great place for a happy hour. Very friendly staff. Just didn't like the burgers we've ordered. Not really tasty. May try something else next time.",
                CreatedOn = DateTime.Now
            };
            var barComment19 = new BarReview
            {
                BarId = bar7.Id,
                UserId = user2.Id,
                Body = "Everything is good with this venue - except the food.",
                CreatedOn = DateTime.Now
            };
            var barComment20 = new BarReview
            {
                BarId = bar7.Id,
                UserId = user3.Id,
                Body = "We instantly felt welcome as soon as we walked in the door. The food was amazing and the staff were very warm and friendly and nothing was  too much trouble. The decor is stunning and I thoroughly recommend this bar/restaurant. We will definitely be going back.",
                CreatedOn = DateTime.Now
            };
            var barComment21 = new BarReview
            {
                BarId = bar8.Id,
                UserId = user4.Id,
                Body = "Quite good.",
                CreatedOn = DateTime.Now
            };
            var barComment22 = new BarReview
            {
                BarId = bar8.Id,
                UserId = user5.Id,
                Body = "Good lunchtime menu.. tasty meal, good price, great service and environment. Separate area for food or just drinks",
                CreatedOn = DateTime.Now
            };
            var barComment23 = new BarReview
            {
                BarId = bar9.Id,
                UserId = user6.Id,
                Body = "I really like this place and had my birthday drinks here after work.  Staff are accommodating, reasonable priced and the outside seating is great",
                CreatedOn = DateTime.Now
            };
            var barComment24 = new BarReview
            {
                BarId = bar9.Id,
                UserId = user7.Id,
                Body = "One of my favourite bars!",
                CreatedOn = DateTime.Now
            };
            var barComment25 = new BarReview
            {
                BarId = bar10.Id,
                UserId = user1.Id,
                Body = "Good place, a bit expensive though!",
                CreatedOn = DateTime.Now
            };
            var barComment26 = new BarReview
            {
                BarId = bar10.Id,
                UserId = user2.Id,
                Body = "Nice inside with a reel modern feel. Cocktails were great!",
                CreatedOn = DateTime.Now
            };
            var barComment27 = new BarReview
            {
                BarId = bar10.Id,
                UserId = user3.Id,
                Body = "Stylish and great outdoor areas. Expensive.",
                CreatedOn = DateTime.Now
            };

            builder.Entity<BarReview>().HasData(barComment1, barComment2, barComment3, barComment4, barComment5, barComment6, barComment7, barComment8, barComment9, barComment10, barComment11, barComment12, barComment13, barComment14, barComment15, barComment16, barComment17, barComment18, barComment19, barComment20, barComment21, barComment22, barComment23, barComment24, barComment25, barComment26, barComment27);

            //SEEDING BARRATINGS
            var barRating1 = new BarRating
            {
                UserId = user1.Id,
                BarId = bar1.Id,
                Value = 4,
                CreatedOn = DateTime.Now
            };
            var barRating2 = new BarRating
            {
                UserId = user2.Id,
                BarId = bar2.Id,
                Value = 5,
                CreatedOn = DateTime.Now
            };
            var barRating3 = new BarRating
            {
                UserId = user3.Id,
                BarId = bar3.Id,
                Value = 4,
                CreatedOn = DateTime.Now
            };
            var barRating4 = new BarRating
            {
                UserId = user4.Id,
                BarId = bar4.Id,
                Value = 4,
                CreatedOn = DateTime.Now
            };
            var barRating5 = new BarRating
            {
                UserId = user5.Id,
                BarId = bar5.Id,
                Value = 4,
                CreatedOn = DateTime.Now
            };
            var barRating6 = new BarRating
            {
                UserId = user6.Id,
                BarId = bar6.Id,
                Value = 2,
                CreatedOn = DateTime.Now
            };
            var barRating7 = new BarRating
            {
                UserId = user7.Id,
                BarId = bar7.Id,
                Value = 4,
                CreatedOn = DateTime.Now
            };
            var barRating8 = new BarRating
            {
                UserId = user1.Id,
                BarId = bar8.Id,
                Value = 4,
                CreatedOn = DateTime.Now
            };
            var barRating9 = new BarRating
            {
                UserId = user2.Id,
                BarId = bar9.Id,
                Value = 3,
                CreatedOn = DateTime.Now
            };

            builder.Entity<BarRating>().HasData(barRating1, barRating2, barRating3, barRating4, barRating5, barRating6, barRating7, barRating8, barRating9);

            //SEEDING COCKTAILRATINGS
            var cocktailRating1 = new CoctailRating
            {
                UserId = user1.Id,
                CoctailId = whiteSangria.Id,
                Value = 4,
                CreatedOn = DateTime.Now
            };
            var cocktailRating2 = new CoctailRating
            {
                UserId = user2.Id,
                CoctailId = michelada.Id,
                Value = 5,
                CreatedOn = DateTime.Now
            };
            var cocktailRating3 = new CoctailRating
            {
                UserId = user3.Id,
                CoctailId = pinkGinIcedTea.Id,
                Value = 4,
                CreatedOn = DateTime.Now
            };
            var cocktailRating4 = new CoctailRating
            {
                UserId = user4.Id,
                CoctailId = longIsland.Id,
                Value = 4,
                CreatedOn = DateTime.Now
            };
            var cocktailRating5 = new CoctailRating
            {
                UserId = user5.Id,
                CoctailId = cranberryVodka.Id,
                Value = 4,
                CreatedOn = DateTime.Now
            };
            var cocktailRating6 = new CoctailRating
            {
                UserId = user6.Id,
                CoctailId = bucksFizz.Id,
                Value = 2,
                CreatedOn = DateTime.Now
            };
            var cocktailRating7 = new CoctailRating
            {
                UserId = user7.Id,
                CoctailId = sexOnTheBeach.Id,
                Value = 4,
                CreatedOn = DateTime.Now
            };
            var cocktailRating8 = new CoctailRating
            {
                UserId = user1.Id,
                CoctailId = raspberryGin.Id,
                Value = 4,
                CreatedOn = DateTime.Now
            };
            var cocktailRating9 = new CoctailRating
            {
                UserId = user2.Id,
                CoctailId = passionFruit.Id,
                Value = 3,
                CreatedOn = DateTime.Now
            };

            builder.Entity<CoctailRating>().HasData(cocktailRating1, cocktailRating2, cocktailRating3, cocktailRating4, cocktailRating5, cocktailRating6, cocktailRating7, cocktailRating8, cocktailRating9);

        }
    }
}
