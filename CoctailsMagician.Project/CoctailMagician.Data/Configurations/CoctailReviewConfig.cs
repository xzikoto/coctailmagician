﻿using System;
using CoctailMagician.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CoctailMagician.Data.Configurations
{
    public class CoctailReviewConfig : IEntityTypeConfiguration<CoctailReview>
    {
        public void Configure(EntityTypeBuilder<CoctailReview> builder)
        {
            builder.HasKey(coctailReview => new
            {
                coctailReview.CoctailId,
                coctailReview.UserId
            });
        }
    }
}
