﻿using CoctailMagician.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CoctailMagician.Data.Configurations
{
    public class BarReviewConfig: IEntityTypeConfiguration<BarReview>
    {
        public void Configure(EntityTypeBuilder<BarReview> builder) 
        {
            builder.HasKey(barReview => new
            {
                barReview.UserId,
                barReview.BarId
            });
        }
    }
}
