﻿using CoctailMagician.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CoctailMagician.Data.Configurations
{
    public class BarRatingConfig: IEntityTypeConfiguration<BarRating>
    {
        public void Configure(EntityTypeBuilder<BarRating> builder) 
        {
            builder.HasKey(barRating => new
            {
                barRating.BarId,
                barRating.UserId
            });
        }
    }
}
