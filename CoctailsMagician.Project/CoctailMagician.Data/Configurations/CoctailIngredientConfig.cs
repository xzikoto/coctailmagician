﻿using CoctailMagician.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoctailMagician.Data.Configurations
{
    public class CoctailIngredientConfig : IEntityTypeConfiguration<CoctailIngredient>
    {
        public void Configure(EntityTypeBuilder<CoctailIngredient> builder)
        {
            builder.HasKey(coctailIngredient => new
            {
                coctailIngredient.CoctailId,
                coctailIngredient.IngredientId
            });
        }
    }
}
