﻿using CoctailMagician.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoctailMagician.Data.Configurations
{
    public class BarsCoctailsConfig : IEntityTypeConfiguration<BarsCoctails>
    {
        public void Configure(EntityTypeBuilder<BarsCoctails> builder)
        {
            builder.HasKey(bc => new
            {
                bc.CoctailId,
                bc.BarId
            });
        }
    }
}
