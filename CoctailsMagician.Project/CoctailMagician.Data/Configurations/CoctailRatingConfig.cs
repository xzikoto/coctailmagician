﻿using CoctailMagician.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoctailMagician.Data.Configurations
{
    public class CoctailRatingConfig : IEntityTypeConfiguration<CoctailRating>
    {
        public void Configure(EntityTypeBuilder<CoctailRating> builder)
        {
            builder.HasKey(coctailRating => new
            {
                coctailRating.UserId,
                coctailRating.CoctailId
            });
        }
    }
}
