﻿using CoctailMagician.Data.Configurations;
using CoctailMagician.Data.Entities;
using CoctailMagician.Data.Seeder;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace CoctailMagician.Data.Context
{
    public class CoctailMagicianContext : IdentityDbContext<User, Role, int>
    {
        public CoctailMagicianContext(DbContextOptions<CoctailMagicianContext> options)
            : base(options)
        {

        }

        public DbSet<Bar> Bars { get; set; }
        public DbSet<BarsCoctails> BarsCoctails { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<Coctail> Coctails { get; set; }
        public DbSet<CoctailReview> CoctailReviews { get; set; }
        public DbSet<CoctailRating> CoctailRatings { get; set; }
        public DbSet<BarReview> BarReviews { get; set; }
        public DbSet<BarRating> BarRatings { get; set; }
        public DbSet<CoctailIngredient> CoctailIngredients { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new BarsCoctailsConfig());
            builder.ApplyConfiguration(new CoctailRatingConfig());
            builder.ApplyConfiguration(new CoctailReviewConfig());
            builder.ApplyConfiguration(new BarRatingConfig());
            builder.ApplyConfiguration(new BarReviewConfig());
            builder.ApplyConfiguration(new CoctailIngredientConfig());

            builder.Seeder();
            base.OnModelCreating(builder);
        }

    }
}
