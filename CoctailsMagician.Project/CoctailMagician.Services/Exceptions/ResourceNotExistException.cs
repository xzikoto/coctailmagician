﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoctailMagician.Services.Exceptions
{
    public class ResourceNotExistException : Exception
    {
        public ResourceNotExistException(string message)
        {

        }

        public ResourceNotExistException()
        {

        }
    }
}
