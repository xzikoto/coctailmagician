﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoctailMagician.Services.Providers
{
    public class DateTimeProvider : Contracts.IDateTimeProvider
    {
        public DateTime GetDateTime() => DateTime.UtcNow;
    }
}
