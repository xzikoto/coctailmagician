﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoctailMagician.Services.Providers.Contracts
{
    public interface IDateTimeProvider
    {
        DateTime GetDateTime();
    }
}
