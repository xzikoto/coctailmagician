﻿using AutoMapper;
using CoctailMagician.Data.Context;
using CoctailMagician.Data.Entities;
using CoctailMagician.Services.Contracts;
using CoctailMagician.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoctailMagician.Services.Services
{
    public class CoctailIngredientService : ICoctailIngredientService
    {
        private readonly CoctailMagicianContext dbcontext;
        private readonly IMapper mapper;

        public CoctailIngredientService(CoctailMagicianContext dbcontext, IMapper mapper)
        {
            this.dbcontext = dbcontext ?? throw new ArgumentNullException(nameof(dbcontext));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public CoctailIngredientDTO CreateCoctailIngredient(int coctailId, int ingredientId)
        {
            var coctailIngredient = new CoctailIngredient
            {
                CoctailId = coctailId,
                IngredientId = ingredientId
            };

            dbcontext.CoctailIngredients.Add(coctailIngredient);
            dbcontext.SaveChanges();

            return mapper.Map<CoctailIngredientDTO>(coctailIngredient);
        }
    }
}
