﻿using AutoMapper;
using CoctailMagician.Data.Context;
using CoctailMagician.Data.Entities;
using CoctailMagician.Services.Contracts;
using CoctailMagician.Services.DTOs;
using CoctailMagician.Services.Enums;
using CoctailMagician.Services.Exceptions;
using CoctailMagician.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace CoctailMagician.Services.Services
{
    public class BarService : IBarService
    {
        private readonly IMapper mapper;
        private readonly CoctailMagicianContext dbcontext;
        private readonly IDateTimeProvider dateTimeProvider;

        public BarService(IMapper mapper,
                          CoctailMagicianContext dbcontext,
                          IDateTimeProvider dateTimeProvider)
        {
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.dbcontext = dbcontext ?? throw new ArgumentNullException(nameof(dbcontext));
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
        }


        public BarDTO GetBar(int id)
        {
            var bar = dbcontext.Bars
                               .Where(bar => bar.IsDeleted == false)
                               .FirstOrDefault(bar => bar.Id == id);

            var barDTO = mapper.Map<BarDTO>(bar);

            return barDTO;
        }

        public ICollection<BarDTO> GetAllBars()
        {
            var barsDTOs = dbcontext.Bars
                                    .Where(bar => bar.IsDeleted == false)
                                    .Select(bar => mapper.Map<BarDTO>(bar))
                                    .ToList();

            return barsDTOs;
        }

        public BarDTO CreateBar(BarDTO barDTO)
        {
            ValidateBarNameNotExists(barDTO.Name);

            var bar = mapper.Map<Bar>(barDTO);
            bar.CreatedOn = dateTimeProvider.GetDateTime();

            dbcontext.Bars.Add(bar);

            try
            {
                dbcontext.SaveChanges();
            }
            catch (ValidationException e)
            {
                throw new InvalidParameterException(e.Message);
            }

            return mapper.Map<BarDTO>(bar);
        }

        public void UpdateBar(BarDTO barDTO)
        {
            var bar = TryGetBar(barDTO.Id);

            ValidateBarNameNotExists(barDTO.Name);
            ValidateBarLocationNotDuplicates(barDTO.Location);

            bar.Name = barDTO.Name;
            bar.Location = barDTO.Location;
            bar.ModifiedOn = dateTimeProvider.GetDateTime();

            dbcontext.Bars.Update(bar);

            try
            {
                dbcontext.SaveChanges();
            }
            catch (ValidationException b)
            {
                throw new InvalidParameterException(b.Message);
            }
        }

        public void DeleteBar(int id)
        {
            var bar = TryGetBar(id);
            ValidateBarIsNotUsed(bar.Id);

            bar.IsDeleted = true;
            bar.ModifiedOn = dateTimeProvider.GetDateTime();
            bar.DeletedOn = dateTimeProvider.GetDateTime();

            dbcontext.Bars.Update(bar);
            dbcontext.SaveChanges();
        }

        private void ValidateBarIsNotUsed(int id)
        {
            //When we finalize the entitities thenn we will finish this
        }

        private void ValidateBarLocationNotDuplicates(string location)
        {
            bool barLocationDuplicates = dbcontext.Bars
                                                  .Any(bar => bar.Location == location);

            if (barLocationDuplicates)
            {
                throw new ResourseInUseException();
            }
        }

        private void ValidateBarNameNotExists(string barName)
        {
            bool barNameExists = dbcontext.Bars
                                          .Any(bar => bar.Name == barName);

            if (barNameExists)
            {
                throw new ResourseInUseException();
            }
        }

        private Bar TryGetBar(int id)
        {
            var bar = dbcontext.Bars
                               .Where(bar => bar.IsDeleted == false)
                               .FirstOrDefault(bar => bar.Id == id);

            if (bar == null)
            {
                throw new ArgumentNullException();
            }

            return bar;
        }

        public ICollection<BarDTO> GetThreeTopRatedBars()
        {
            var topBars = dbcontext.Bars.Where(coctail => coctail.IsDeleted == false).Take(3);

            return topBars.Select(coctail => mapper.Map<BarDTO>(coctail)).ToList();
        }

        public ICollection<BarDTO> ListBars(int fromIndex, int amount, string searchCriteria, SortBy? sortBy, Order? order, out int totalBarsCount)
        {
            var query = dbcontext.Bars.Include(bar => bar.Ratings).Where(bar => bar.IsDeleted == false);

            query = Search(query, searchCriteria);
            totalBarsCount = query.Count();

            query = SortBars(query, sortBy, order);

            query = query.Skip(fromIndex).Take(amount);
            return query.Select(bar => mapper.Map<BarDTO>(bar)).ToList();
        }

        private IQueryable<Bar> SortBars(IQueryable<Bar> query, SortBy? sortBy, Order? order)
        {
            var sorter = GetSorter(sortBy);

            if (sortBy != null &&  order==null)
            {
                order = Order.Asc;
            }

            if (order == Order.Asc)
            {
                return query.OrderBy(sorter).AsQueryable();
            }
            else if (order == Order.Desc)
            {
                return query.OrderByDescending(sorter).AsQueryable();
            }
            else
            {
                return query;
            }
        }

        private Func<Bar, object> GetSorter(SortBy? sortBy)
        {
            return (bar) =>
            {
                switch (sortBy)
                {
                    case SortBy.Name:
                        return bar.Name;
                    case SortBy.Location:
                        return bar.Location;
                    case SortBy.Rating:
                        return bar.Ratings;
                    default:
                        return bar.Id;
                }
            };
        }

        private IQueryable<Bar> Search(IQueryable<Bar> query, string keyword)
        {
            if (keyword != null)
            {
                query = query.Where(bar => bar.Name.Contains(keyword) ||
                                           bar.Location.Contains(keyword) ||
                                           bar.Ratings.ToString().Equals(keyword));

            }

            return query;
        }

        private IQueryable<Bar> FilterBars(IQueryable<Bar> query, double? rating, string location)
        {
            if (rating != null)
            {
                query = query.Where(bar => (bar.Ratings.Sum(x => x.Value) / bar.Ratings.Count) == rating);
            }

            if (location != null)
            {
                query = query.Where(bar => bar.Location == location);
            }

            return query;
        }
    }
}
