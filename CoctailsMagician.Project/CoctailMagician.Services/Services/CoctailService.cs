﻿using AutoMapper;
using CoctailMagician.Data.Context;
using CoctailMagician.Data.Entities;
using CoctailMagician.Services.Contracts;
using CoctailMagician.Services.DTOs;
using CoctailMagician.Services.Exceptions;
using CoctailMagician.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace CoctailMagician.Services.Services
{
    public class CoctailService : ICoctailService
    {
        private readonly IMapper mapper;
        private readonly CoctailMagicianContext dbcontext;
        private readonly IDateTimeProvider dateTimeProvider;

        public CoctailService(IMapper mapper,
                          CoctailMagicianContext dbcontext,
                          IDateTimeProvider dateTimeProvider)
        {
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.dbcontext = dbcontext ?? throw new ArgumentNullException(nameof(dbcontext));
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
        }

        /*
         * So the difference between IQueryable and IEnumerable is about where the filter logic is executed. 
         * One executes on the client side and the other executes on the database.

            So if you working with only in-memory data collection IEnumerable is a good choice 
            but if you want to query data collection which is connected with database `IQueryable is a better choice as it reduces network traffic and
            uses the power of SQL language.
         */

        private IQueryable<Coctail> CoctailsQuery
        {
            get => dbcontext.Coctails
                            .Include(coctail => coctail.Ingredients)
                            .Where(coctail => coctail.IsDeleted == false);
        }

        public CoctailDTO GetCoctail(int id)
        {
            var coctail = dbcontext.Coctails
                                   .Where(coctail => coctail.IsDeleted == false)
                                   .FirstOrDefault(coctail => coctail.Id == id);

            var coctailDTO = mapper.Map<CoctailDTO>(coctail);

            return coctailDTO;
        }

        public ICollection<CoctailDTO> GetAllCoctails()
        {
            var coctailsDTO = dbcontext.Coctails
                                       .Include(coctail => coctail.Ingredients)
                                       .Where(coctail => coctail.IsDeleted == false)
                                       .Select(coctail => mapper.Map<CoctailDTO>(coctail))
                                       .ToList();

            return coctailsDTO;
        }

        public CoctailDTO CreateCoctail(CoctailDTO coctailDTO)
        {
            ValidateCoctailNameNotExists(coctailDTO.Name);
            
            ValidateCoctailIngredientsNotExist(coctailDTO);

            var coctail = mapper.Map<Coctail>(coctailDTO);
            
            if (!coctailDTO.Ingredients.Count.Equals(0))
            {
                ValidateCoctailIngredientsDiffers(coctail.Ingredients,coctailDTO.Ingredients);
            }


            coctail.CreatedOn = dateTimeProvider.GetDateTime();
            
            dbcontext.Coctails.Add(coctail);
            dbcontext.SaveChanges();

            if (!coctail.Ingredients.Count.Equals(0))
            {
                SetCoctailIngredients(mapper.Map<CoctailDTO>(coctail));
            }

            try
            {
                dbcontext.SaveChanges();
            }
            catch (ValidationException e)
            {
                throw new InvalidParameterException(e.Message);
            }

            return mapper.Map<CoctailDTO>(coctail);
        }

        public void UpdateCoctail(CoctailDTO coctailDTO)
        {
            var coctail = TryGetCoctail(coctailDTO.Id);

            if (coctailDTO.Name != coctail.Name)
            {
                ValidateCoctailNameNotExists(coctailDTO.Name);
            }

            if (ValidateCoctailIngredientsDiffers(coctail.Ingredients, coctailDTO.Ingredients))
            {
                DeletePreviousIngredientsFromCoctail(coctail.Ingredients);

                SetCoctailIngredients(coctailDTO);

                coctail.Ingredients = mapper.Map<List<CoctailIngredient>>(coctailDTO.Ingredients);
            }

            coctail.Name = coctailDTO.Name;
            coctail.ModifiedOn = dateTimeProvider.GetDateTime();

            try
            {
                dbcontext.SaveChanges();
            }
            catch (ValidationException b)
            {
                throw new InvalidParameterException(b.Message);
            }
        }

        private void DeletePreviousIngredientsFromCoctail(List<CoctailIngredient> ingredients)
        {
            foreach (var item in ingredients)
            {

                var coctailIngredient = dbcontext.CoctailIngredients
                                                 .FirstOrDefault(coctailIngredient => coctailIngredient.CoctailId == item.CoctailId && coctailIngredient.IngredientId == item.IngredientId);
                
                coctailIngredient.isDeleted = true;

                dbcontext.CoctailIngredients.Update(coctailIngredient);
            }

            dbcontext.SaveChanges();
        }

        public void ValidateCoctailIngredientsNotExist(CoctailDTO coctailDTO)
        {
            var listIngredients = coctailDTO.Ingredients;
            
            foreach (var item in listIngredients)
            {
                var coctailIngredientExist = dbcontext.CoctailIngredients
                                                      .Any(coctailIngredient => 
                                                          (coctailIngredient.CoctailId == item.CoctailId && coctailIngredient.IngredientId == item.IngredientId));

                if (coctailIngredientExist)
                {
                    throw new ResourseAlreadyExistException();
                }
            }
        }

        private void SetCoctailIngredients(CoctailDTO coctailDTO)
        {
            List<CoctailIngredient> list = new List<CoctailIngredient>();
            var coctail = mapper.Map<Coctail>(coctailDTO);

            foreach (var item in coctail.Ingredients)
            {
                var ingredient = new CoctailIngredient
                {
                    CoctailId = item.CoctailId,
                    IngredientId = item.IngredientId
                };

                ingredient.Coctail = item.Coctail;
                ingredient.Ingredient = item.Ingredient;
                list.Add(ingredient);
            }

            coctail.Ingredients = list;
        }

        private bool ValidateCoctailIngredientsDiffers(ICollection<CoctailIngredient> inputIngredients, ICollection<CoctailIngredientDTO>  oldIngredientsDTO)
        {
            var oldIngredients = oldIngredientsDTO.Select(ingr => mapper.Map<CoctailIngredient>(ingr)).ToList();

            if (inputIngredients.Equals(oldIngredients))
            {
                return false;
            }
            else
            {
                return true;
            }
            
        }

        private void ValidateCoctailNameNotExists(string name)
        {
            var coctailNameExists = dbcontext.Coctails
                                   .Any(coctail => coctail.Name == name);

            if (coctailNameExists)
            {
                throw new ResourseInUseException();
            }
        }

        public void DeleteCoctail(int id)
        {
            var coctail = TryGetCoctail(id);
            ValidateCoctailIsNotDependent(coctail.Id);

            coctail.IsDeleted = true;
            coctail.IsAvailable = false;
            coctail.IsDeleted = true;
            coctail.DeletedOn = dateTimeProvider.GetDateTime();
            coctail.ModifiedOn = dateTimeProvider.GetDateTime();

            dbcontext.Coctails.Update(coctail);
            dbcontext.SaveChanges();
        }

        private void ValidateCoctailIsNotDependent(int id)
        {
            
        }

        private Coctail TryGetCoctail(int id)
        {
            var coctail = dbcontext.Coctails
                               .Include(coctail => coctail.Ingredients)
                               .Where(coctail => coctail.IsDeleted == false)
                               .FirstOrDefault(coctail => coctail.Id == id);

            if (coctail == null)
            {
                throw new ArgumentNullException();
            }

            return coctail;
        }

        public List<IngredientDTO> GetIngredientsOfCoctail(int id)
        {
            var coctailIngredients = dbcontext.CoctailIngredients.Where(coctail => coctail.CoctailId == id).ToList();
            var ingredients = new List<IngredientDTO>();

            foreach (var item in coctailIngredients)
            {
                var ingredient = dbcontext.Ingredients.FirstOrDefault(ingr => ingr.Id == item.IngredientId);

                if (ingredient != null)
                {
                    var ingredientDTO = mapper.Map<IngredientDTO>(ingredient);

                    ingredients.Add(ingredientDTO);
                }
            }

            return ingredients;
        }

        public ICollection<CoctailDTO> GetThreeTopRatedCoctails()
        {
            var query = CoctailsQuery.Where(coctail => coctail.IsDeleted == false).Take(3).AsNoTracking().ToList();

            return query.Select(coctail => mapper.Map<CoctailDTO>(coctail)).ToList();
        }
    }
}
