﻿using AutoMapper;
using CoctailMagician.Data.Context;
using CoctailMagician.Data.Entities;
using CoctailMagician.Services.Contracts;
using CoctailMagician.Services.DTOs;
using CoctailMagician.Services.Exceptions;
using CoctailMagician.Services.Providers.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace CoctailMagician.Services.Services
{
    public class IngredientService : IIngredientService
    {
        private readonly IMapper mapper;
        private readonly IDateTimeProvider dateTimeProvider;
        private readonly CoctailMagicianContext dbcontext;

        public IngredientService(IMapper mapper,
                                 CoctailMagicianContext dbcontext,
                                 IDateTimeProvider dateTimeProvider)
        {
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.dbcontext = dbcontext ?? throw new ArgumentNullException(nameof(dbcontext));
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
        }

        public IngredientDTO CreateIngredient(IngredientDTO ingredientDTO)
        {
            ValidateCoctailNameNotExists(ingredientDTO.Name);
            var ingredient = mapper.Map<Ingredient>(ingredientDTO);

            dbcontext.Ingredients.Add(ingredient);

            try
            {
                dbcontext.SaveChanges();
            }
            catch (ValidationException e)
            {
                throw new InvalidParameterException(e.Message);
            }
            catch(ResourseAlreadyExistException b)
            {
                throw new ResourseAlreadyExistException(b.Message);
            }

            return mapper.Map<IngredientDTO>(ingredient);
        }

        private void ValidateCoctailNameNotExists(string name)
        {
            var nameExists = dbcontext.Ingredients.Any(ingr => ingr.Name == name);

            if (nameExists)
            {
                throw new ResourseAlreadyExistException();
            }
        }

        public void DeleteIngredientr(int id)
        {
            throw new NotImplementedException();
        }

        public ICollection<IngredientDTO> GetAllIngredients()
        {
            var ingredients = dbcontext.Ingredients.ToList();

            var ingredientsDTO = ingredients.Select(ingr => mapper.Map<IngredientDTO>(ingr));

            return ingredientsDTO.ToList();
        }

        public IngredientDTO GetIngredient(int id)
        {
            var ingredient = dbcontext.Ingredients.FirstOrDefault(ingr => ingr.IsDeleted == false && ingr.Id == id);

            var ingredientDTO = mapper.Map<IngredientDTO>(ingredient);

            return ingredientDTO;
        }

        public void UpdateIngredient(IngredientDTO barDTO)
        {
            throw new NotImplementedException();
        }
    }
}
