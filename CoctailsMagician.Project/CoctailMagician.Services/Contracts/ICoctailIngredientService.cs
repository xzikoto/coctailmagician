﻿using CoctailMagician.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoctailMagician.Services.Contracts
{
    public interface ICoctailIngredientService
    {
        CoctailIngredientDTO CreateCoctailIngredient(int coctailId, int ingredientId);
    }
}
