﻿using CoctailMagician.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoctailMagician.Services.Contracts
{
    public interface IIngredientService
    {
        IngredientDTO GetIngredient(int id);
        ICollection<IngredientDTO> GetAllIngredients();
        IngredientDTO CreateIngredient(IngredientDTO barDTO);
        void UpdateIngredient(IngredientDTO barDTO);
        void DeleteIngredientr(int id);
    }
}
