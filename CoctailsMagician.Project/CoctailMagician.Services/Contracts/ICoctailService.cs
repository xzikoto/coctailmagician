﻿using CoctailMagician.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoctailMagician.Services.Contracts
{
    public interface ICoctailService
    {
        CoctailDTO GetCoctail(int id);
        ICollection<CoctailDTO> GetAllCoctails();
        CoctailDTO CreateCoctail(CoctailDTO coctailDTO);
        void UpdateCoctail(CoctailDTO coctailDTO);
        void DeleteCoctail(int id);
        List<IngredientDTO> GetIngredientsOfCoctail(int id);
        ICollection<CoctailDTO> GetThreeTopRatedCoctails();
    }
}
