﻿using CoctailMagician.Services.DTOs;
using CoctailMagician.Services.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoctailMagician.Services.Contracts
{
    public interface IBarService
    {
        BarDTO GetBar(int id);
        ICollection<BarDTO> GetAllBars();
        BarDTO CreateBar(BarDTO barDTO);
        void UpdateBar(BarDTO barDTO);
        void DeleteBar(int id);
        ICollection<BarDTO> GetThreeTopRatedBars();
        ICollection<BarDTO> ListBars(int fromIndex, int amount, string searchCriteria, SortBy? sortBy, Order? order, out int totalBarsCount);
    }
}
