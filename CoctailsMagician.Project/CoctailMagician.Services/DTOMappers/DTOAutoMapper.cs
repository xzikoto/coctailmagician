﻿using AutoMapper;
using CoctailMagician.Data.Entities;
using CoctailMagician.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoctailMagician.Services.DTOMappers
{
    public class DTOAutoMapper: Profile
    {
        public DTOAutoMapper()
        {
            CreateMap<Bar, BarDTO>().ReverseMap();
            CreateMap<BarRating, BarRatingDTO>().ReverseMap();
            CreateMap<BarReview, BarReview>().ReverseMap();

            CreateMap<Coctail, CoctailDTO>().ReverseMap();
            CreateMap<CoctailRating, CoctailRatingDTO>().ReverseMap();
            CreateMap<CoctailReview, CoctailReviewDTO>().ReverseMap();

            CreateMap<Ingredient, IngredientDTO>().ReverseMap();

            CreateMap<CoctailIngredient, CoctailIngredientDTO>()
                .ForMember(dest => dest.CoctailId, opt => opt.MapFrom(src => src.CoctailId))
                .ForMember(dest => dest.IngredientId, opt => opt.MapFrom(src => src.IngredientId))
                .ReverseMap();

            //CreateMap<ICollection<CoctailIngredient>, ICollection<CoctailIngredientDTO>>();

            //DISCLAIMER - BIG INCONSISTENCY BETWEEN (Coctail,Bar)'s properties
            //AND  (Coctail,Bar)DTO's properties
        }
    }
}
