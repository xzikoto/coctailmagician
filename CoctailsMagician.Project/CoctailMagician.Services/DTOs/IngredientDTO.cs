﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoctailMagician.Services.DTOs
{
    public class IngredientDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
