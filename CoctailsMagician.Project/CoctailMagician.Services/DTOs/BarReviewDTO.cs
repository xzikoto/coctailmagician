﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoctailMagician.Services.DTOs
{
    public class BarReviewDTO
    {
        public int UserId { get; set; }
        public UserDTO User { get; set; }
        public int BarId { get; set; }
        public string Review { get; set; }
    }
}
