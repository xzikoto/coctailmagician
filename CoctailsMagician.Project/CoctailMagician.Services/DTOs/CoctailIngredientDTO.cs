﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoctailMagician.Services.DTOs
{
    public class CoctailIngredientDTO
    {
        public CoctailDTO Coctail { get; set; }
        public int CoctailId { get; set; }

        public IngredientDTO Ingredient { get; set; }
        public int IngredientId { get; set; }

        public override bool Equals(object obj)
        {
            var coctailIngredientDTO = obj as CoctailIngredientDTO;

            if (coctailIngredientDTO == null)
            {
                return false;
            }

            return this.IngredientId == coctailIngredientDTO.IngredientId && this.CoctailId == coctailIngredientDTO.CoctailId;
        }
    }
}
