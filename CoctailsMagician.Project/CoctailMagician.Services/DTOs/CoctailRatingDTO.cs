﻿namespace CoctailMagician.Services.DTOs
{
    public class CoctailRatingDTO
    {
        public int UserId { get; set; }
        public int CoctailId { get; set; }
        public byte Rating { get; set; }
    }
}