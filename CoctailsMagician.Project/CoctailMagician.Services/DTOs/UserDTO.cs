﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoctailMagician.Services.DTOs
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
    }
}
