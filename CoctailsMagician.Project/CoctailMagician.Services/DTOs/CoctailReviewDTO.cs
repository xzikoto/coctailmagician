﻿namespace CoctailMagician.Services.DTOs
{
    public class CoctailReviewDTO
    {
        public int UserId { get; set; }
        public UserDTO User { get; set; }
        public int CoctailId { get; set; }
        public string Review { get; set; }
    }
}