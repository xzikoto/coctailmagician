﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoctailMagician.Services.DTOs
{
    public class CoctailDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<CoctailIngredientDTO> Ingredients { get; set; }

        //public ICollection<BarDTO> Bars { get; set; }
        //public ICollection<CoctailRatingDTO> Ratings { get; set; }
        //public ICollection<CoctailReviewDTO> Reviews { get; set; }
    }
}
