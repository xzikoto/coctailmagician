﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoctailMagician.Services.DTOs
{
    public class BarRatingDTO
    {
        public int UserId { get; set; }
        public int BarId { get; set; }
        public byte Rating { get; set; }
    }
}
