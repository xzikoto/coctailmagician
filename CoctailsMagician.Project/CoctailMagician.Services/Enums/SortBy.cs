﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoctailMagician.Services.Enums
{
    public enum SortBy
    {
        Name,
        Location,
        Rating
    }
}
