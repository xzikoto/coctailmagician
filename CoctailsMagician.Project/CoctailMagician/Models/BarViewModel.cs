﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoctailMagician.Web.Models
{
    public class BarViewModel
    {
        public int Id { get; set; }

        [Required]
        [MinLength(3), MaxLength(128)]
        [Display(Name = "Place")]
        public string Name { get; set; }

        [Required]
        [MinLength(3), MaxLength(128)]
        [Display(Name = "Location")]
        public string Location { get; set; }
        
        [Display(Name = "Rating")]
        public double? Rating { get; set; }
    }
}
