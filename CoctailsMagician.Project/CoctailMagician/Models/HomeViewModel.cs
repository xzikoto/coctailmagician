﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoctailMagician.Web.Models
{
    public class HomeViewModel
    {
        public ICollection<CoctailViewModel> TopRatedCoctails { get; set; }
        public ICollection<BarViewModel> TopRatedBars { get; set; }
    }
}
