﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoctailMagician.Web.Models
{
    public class IngredientUnitViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
