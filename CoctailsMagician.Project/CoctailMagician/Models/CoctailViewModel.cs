﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoctailMagician.Web.Models
{
    public class CoctailViewModel
    {
        public int Id { get; set; }

        [Required]
        [MinLength(3), MaxLength(128)]
        [Display(Name = "Place")]
        public string Name { get; set; }
        
        public ICollection<IngredientViewModel> Ingredients { get; set; }
    }
}
