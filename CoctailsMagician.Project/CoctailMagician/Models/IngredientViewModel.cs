﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoctailMagician.Web.Models
{
    public class IngredientViewModel
    {
        public int CoctailId { get; set; }
        public int IngredientId { get; set; }
    }
}
