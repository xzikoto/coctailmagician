﻿using AutoMapper;
using CoctailMagician.Services.Contracts;
using CoctailMagician.Services.DTOs;
using CoctailMagician.Services.Exceptions;
using CoctailMagician.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoctailMagician.Web.ApiControllers
{
    [Route("api/ingredients")]
    [ApiController]
    public class IngredientApiController : ControllerBase
    {
        private readonly IIngredientService ingredientService;
        private readonly IMapper mapper;

        public IngredientApiController(IIngredientService ingredientService, IMapper mapper)
        {
            this.ingredientService = ingredientService ?? throw new ArgumentNullException(nameof(ingredientService));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpGet]
        public IActionResult ListIngredients()
        {
            var models = this.ingredientService.GetAllIngredients().Select(ingr => mapper.Map<IngredientUnitViewModel>(ingr));

            return Ok(models);
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult GetIngredient(int id)
        {
            var coctailDTO = this.ingredientService.GetIngredient(id);

            if (coctailDTO == null)
            {
                return NotFound();
            }
            else
            {
                var model = mapper.Map<IngredientUnitViewModel>(coctailDTO);
                return Ok(model);
            }
        }

        [HttpPost]
        public IActionResult CreateIngredient(IngredientUnitViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var ingredientDTO = mapper.Map<IngredientDTO>(model);

            try
            {
                var newIngredient = ingredientService.CreateIngredient(ingredientDTO);
                var ingredientViewModel = mapper.Map<IngredientUnitViewModel>(newIngredient);

                return Created("CreateCoctail", ingredientViewModel);
            }
            catch (ResourseAlreadyExistException)
            {
                return Conflict();
            }
            catch (InvalidParameterException e)
            {
                return BadRequest(e.Message);
            }
        }

    }
}
