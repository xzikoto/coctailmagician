﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CoctailMagician.Services.Contracts;
using CoctailMagician.Services.DTOs;
using CoctailMagician.Services.Exceptions;
using CoctailMagician.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoctailMagician.Web.ApiControllers
{
    [Route("api/coctails")]
    [ApiController]
    public class CoctailApiController : ControllerBase
    {
        private readonly ICoctailService coctailService;
        private readonly IMapper mapper;

        public CoctailApiController(ICoctailService coctailService, IMapper mapper)
        {
            this.coctailService = coctailService ?? throw new ArgumentNullException(nameof(coctailService));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpGet]
        public IActionResult ListCoctails()
        {
            var models = this.coctailService.GetAllCoctails()
                                            .Select(coctail => mapper.Map<CoctailViewModel>(coctail))
                                            .ToList();
            return Ok(models);
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult GetCoctail(int id)
        {
            var coctailDTO = this.coctailService.GetCoctail(id);

            if (coctailDTO == null)
            {
                return NotFound();
            }
            else
            {
                var model = mapper.Map<CoctailViewModel>(coctailDTO);
                return Ok(model);
            }
        }

        [HttpPost]
        public  IActionResult CreateCoctail(CoctailViewModel model) 
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var coctailDTO = mapper.Map<CoctailDTO>(model);

            try
            {
                var newCoctail = coctailService.CreateCoctail(coctailDTO);
                var coctailViewModel = mapper.Map<CoctailViewModel>(newCoctail);

                return Created("CreateCoctail", coctailViewModel);
            }
            catch (ResourseAlreadyExistException)
            {
                return Conflict();
            }
            catch (InvalidParameterException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut("{id}")]
        public IActionResult UpdateCoctail(int id, [FromBody] CoctailViewModel coctailViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var coctailDTO = mapper.Map<CoctailDTO>(coctailViewModel);
                coctailDTO.Id = id;

                coctailService.UpdateCoctail(coctailDTO);
                return NoContent();
            }
            catch (ResourceNotExistException)
            {
                return NotFound();
            }
            catch (ResourseAlreadyExistException)
            {
                return Conflict();
            }
            catch (InvalidParameterException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult DeleteCoctail(int id)
        {
            try
            {
                this.coctailService.DeleteCoctail(id);
                return NoContent();
            }
            catch (ResourceNotExistException e )
            {
                return NotFound(e.Message);
            }
        }

    }
}
