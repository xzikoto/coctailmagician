﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CoctailMagician.Services.Contracts;
using CoctailMagician.Services.DTOs;
using CoctailMagician.Services.Exceptions;
using CoctailMagician.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoctailMagician.Web.ApiControllers
{
    [Route("api/bars")]
    [ApiController]
    public class BarApiController : ControllerBase
    {
        private readonly IBarService barService;
        private readonly IMapper mapper;

        public BarApiController(IBarService barService, IMapper mapper)
        {
            this.barService = barService ?? throw new ArgumentNullException(nameof(barService));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpGet]
        public IActionResult ListBars()
        {
            var models = this.barService.GetAllBars()
                                        .Select(bar => mapper.Map<BarViewModel>(bar))
                                        .ToList();

            return Ok(models);
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult GetBar(int id)
        {
            var barDTO = this.barService.GetBar(id);

            if (barDTO == null)
            {
                return NotFound();
            }
            else
            {
                var model = mapper.Map<BarViewModel>(barDTO);
                return Ok(model);
            }
        }

        [HttpPost]
        [Route("")]
        public IActionResult CreateBar(BarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var barDTO = mapper.Map<BarDTO>(model);

            try
            {
                var newBar = barService.CreateBar(barDTO);
                var barViewModel = mapper.Map<BarViewModel>(newBar);

                return Created("CreateCountry", barViewModel);
            }
            catch (ResourseAlreadyExistException)
            {
                return Conflict();
            }
            catch (InvalidParameterException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut("{id}")]
        public IActionResult UpdateBar(int id, [FromBody] BarViewModel barViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var barDTO = mapper.Map<BarDTO>(barViewModel);
                barDTO.Id = id;

                barService.UpdateBar(barDTO);
                return NoContent();
            }
            catch (ResourceNotExistException)
            {
                return NotFound();
            }
            catch (ResourseAlreadyExistException)
            {
                return Conflict();
            }
            catch (InvalidParameterException e)
            {
                return BadRequest(e.Message);
            }
        }



        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                barService.DeleteBar(id);
                return NoContent();
            }
            catch (ResourceNotExistException)
            {
                return NotFound();
            }
            catch (ResourseInUseException)
            {
                return Conflict();
            }
        }
    }
}
