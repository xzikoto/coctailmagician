﻿using AutoMapper;
using CoctailMagician.Models;
using CoctailMagician.Services.Contracts;
using CoctailMagician.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace CoctailMagician.Controllers
{
    public class HomeController : Controller
    {
        private readonly IMapper mapper;
        private readonly IBarService barService;
        private readonly ICoctailService coctailService;
        private readonly ILogger<HomeController> _logger;


        public HomeController(ILogger<HomeController> logger, IBarService barService,
                              ICoctailService coctailService, IMapper mapper)
        {
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.barService = barService ?? throw new ArgumentNullException(nameof(barService));
            this.coctailService = coctailService ?? throw new ArgumentNullException(nameof(coctailService));
            _logger = logger;
        }

        public IActionResult Index()
        {
            var topThreeRatedCoctailsDTO = mapper.Map<ICollection<CoctailViewModel>>(this.coctailService.GetThreeTopRatedCoctails());
            var topThreeRatedBarsDTO = mapper.Map<ICollection<BarViewModel>>(this.barService.GetThreeTopRatedBars());

            var homeViewModel = new HomeViewModel
            {
                TopRatedCoctails = topThreeRatedCoctailsDTO,
                TopRatedBars = topThreeRatedBarsDTO
            };

            if (homeViewModel == null)
            {
                return NotFound();
            }

            return View(homeViewModel);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
