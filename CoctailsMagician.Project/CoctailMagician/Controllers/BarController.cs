﻿using AutoMapper;
using CoctailMagician.Services.Contracts;
using CoctailMagician.Services.Enums;
using CoctailMagician.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using X.PagedList;

namespace CoctailMagician.Web.Controllers
{
    public class BarController : Controller
    {
        private readonly IBarService barService;
        private readonly IMapper mapper;

        public BarController(IBarService barService, IMapper mapper)
        {
            this.barService = barService ?? throw new ArgumentNullException(nameof(barService));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public IActionResult Index(string search, int? page, SortBy? sortBy, Order? orderBy )
        {
            ViewBag.Search = search;
            ViewBag.SortBy = sortBy;
            ViewBag.Order = orderBy;

            int pageNumber = (page ?? 1);
            int pageSize = 20;

            int totalBarsCount;
            var fromIndex = (pageNumber - 1) * pageSize;
            var barsDTOs = barService.ListBars(fromIndex, pageSize, search, sortBy, orderBy, out totalBarsCount);

            var barModels = barsDTOs.Select(dto => mapper.Map<BarViewModel>(dto));
            var barsPagedList = new StaticPagedList<BarViewModel>(barModels, pageNumber, pageSize, totalBarsCount);

            return View(barsPagedList);
        }
    }
}
