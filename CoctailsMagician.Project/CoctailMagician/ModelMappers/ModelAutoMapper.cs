﻿using AutoMapper;
using CoctailMagician.Services.DTOs;
using CoctailMagician.Web.Models;
namespace CoctailMagician.Web.ModelMappers
{
    public class ModelAutoMapper: Profile
    {
        public ModelAutoMapper()
        {
            CreateMap<BarDTO, BarViewModel>().ReverseMap();
            CreateMap<CoctailDTO, CoctailViewModel>().ReverseMap();
            CreateMap<CoctailIngredientDTO, IngredientViewModel>().ReverseMap();
            CreateMap<IngredientDTO, IngredientUnitViewModel>().ReverseMap();
        }
    }
}
